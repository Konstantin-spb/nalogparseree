package entity;

public class EntityRazdel8 {

//    <КнПокСтр НомерПор="111" НомСчФПрод="222" ДатаСчФПрод="02.03.2015" НомИспрСчФ="333" ДатаИспрСчФ="03.03.2015" НомКСчФПрод="444"
// ДатаКСчФПрод="04.03.2015" НомИспрКСчФ="555" ДатаИспрКСчФ="06.03.2015" НомТД="888" ОКВ="643" СтоимПокупВ="66.60" СумНДСВыч="7">
//    <КодВидОпер>01</КодВидОпер>
//    <ДокПдтвУпл НомДокПдтвУпл="777" ДатаДокПдтвУпл="27.02.2015" />
//    <ДатаУчТов>25.02.2015</ДатаУчТов>
//    <СвПрод>
//    <СведЮЛ ИННЮЛ="2452041366" КПП="245201001" />
//    </СвПрод>
//    <СвПос>
//    <СведЮЛ ИННЮЛ="5403364000" КПП="540301001" />
//    </СвПос>
//    </КнПокСтр>


    int nomerPor; //Порядковый номер
    String nomSchFProd; //Номер счёт-фактуры
    String dataSchFProd; //Дата счёт-фактуры
    String nomIspSchF; //Номер исправления счёт-фактуры
    String dataIsprSchF;
    String nomKSchFProd;
    String dataKSchFProd;
    String nomIsprKSchF;
    String dataIsprKSchF;
    String nomTD;
    String okv = "643";
    String stoimPokupV;
    String sumNdsVych;

    String kodVidOper;

    String nomAndDatePdtvUpl;

    String nomDokPdtvUpl;
    String dataDokPdtvUpl;

    String dataUchTov;

    //Продавец
    String innProd;
    String kppProd;

    //Посредник
    String innPosr;
    String kppPosr;

    public int getNomerPor() {
        return nomerPor;
    }

    public void setNomerPor(int nomerPor) {
        this.nomerPor = nomerPor;
    }

    public String getNomSchFProd() {
        return nomSchFProd;
    }

    public void setNomSchFProd(String nomSchFProd) {
        this.nomSchFProd = nomSchFProd;
    }

    public String getDataSchFProd() {
        return dataSchFProd;
    }

    public void setDataSchFProd(String dataSchFProd) {
        this.dataSchFProd = dataSchFProd;
    }

    public String getNomIspSchF() {
        return nomIspSchF;
    }

    public void setNomIspSchF(String nomIspSchF) {
        this.nomIspSchF = nomIspSchF;
    }

    public String getDataIsprSchF() {
        return dataIsprSchF;
    }

    public void setDataIsprSchF(String dataIsprSchF) {
        this.dataIsprSchF = dataIsprSchF;
    }

    public String getNomKSchFProd() {
        return nomKSchFProd;
    }

    public void setNomKSchFProd(String nomKSchFProd) {
        this.nomKSchFProd = nomKSchFProd;
    }

    public String getDataKSchFProd() {
        return dataKSchFProd;
    }

    public void setDataKSchFProd(String dataKSchFProd) {
        this.dataKSchFProd = dataKSchFProd;
    }

    public String getNomIsprKSchF() {
        return nomIsprKSchF;
    }

    public void setNomIsprKSchF(String nomIsprKSchF) {
        this.nomIsprKSchF = nomIsprKSchF;
    }

    public String getDataIsprKSchF() {
        return dataIsprKSchF;
    }

    public void setDataIsprKSchF(String dataIsprKSchF) {
        this.dataIsprKSchF = dataIsprKSchF;
    }

    public String getNomTD() {
        return nomTD;
    }

    public void setNomTD(String nomTD) {
        this.nomTD = nomTD;
    }

    public String getOkv() {
        return okv;
    }

    public void setOkv(String okv) {
        this.okv = okv;
    }

    public String getStoimPokupV() {
        return stoimPokupV;
    }

    public void setStoimPokupV(String stoimPokupV) {
        this.stoimPokupV = stoimPokupV;
    }

    public String getSumNdsVych() {
        return sumNdsVych;
    }

    public void setSumNdsVych(String sumNdsVych) {
        this.sumNdsVych = sumNdsVych;
    }

    public String getKodVidOper() {
        return kodVidOper;
    }

    public void setKodVidOper(String kodVidOper) {
        this.kodVidOper = kodVidOper;
    }

    public String getNomDokPdtvUpl() {
        return nomDokPdtvUpl;
    }

    public void setNomDokPdtvUpl(String nomDokPdtvUpl) {
        this.nomDokPdtvUpl = nomDokPdtvUpl;
    }

    public String getDataDokPdtvUpl() {
        return dataDokPdtvUpl;
    }

    public void setDataDokPdtvUpl(String dataDokPdtvUpl) {
        this.dataDokPdtvUpl = dataDokPdtvUpl;
    }

    public String getDataUchTov() {
        return dataUchTov;
    }

    public void setDataUchTov(String dataUchTov) {
        this.dataUchTov = dataUchTov;
    }

    public String getInnProd() {
        return innProd;
    }

    public void setInnProd(String innProd) {
        this.innProd = innProd;
    }

    public String getKppProd() {
        return kppProd;
    }

    public void setKppProd(String kppProd) {
        this.kppProd = kppProd;
    }

    public String getInnPosr() {
        return innPosr;
    }

    public void setInnPosr(String innPosr) {
        this.innPosr = innPosr;
    }

    public String getKppPosr() {
        return kppPosr;
    }

    public void setKppPosr(String kppPosr) {
        this.kppPosr = kppPosr;
    }

    public String getNomAndDatePdtvUpl() {
        return nomAndDatePdtvUpl;
    }

    public void setNomAndDatePdtvUpl(String nomAndDatePdtvUpl) {
        this.nomAndDatePdtvUpl = nomAndDatePdtvUpl;
    }
}
