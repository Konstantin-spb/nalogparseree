package entity;

/**
 * Created by konstantin on 12.08.15.
 */
public class EntityNoNds {

    String idfail = new String("UTF-8");
    String knd;
    String dataDok;
    String period;
    String otchetGod;
    String kodNo;
    String nomKorr;
    String poMestu;
    String okved;
    String tel;
    String naimOrg;
    String innUl;
    String kpp;
    String prPodp;
    String familiya;
    String imya;
    String otchestvo;
    String oktmo;
    String kbk;
    String sumPu1731;
    String nalPu164;
    String nalVosstObsch;
    String nalBaza;
    String sumNal;
    String naimKnPok;
    String naimKnProd;
    String naimZhuchVistSchF;
    String naimZhuchPoluchSchF;
    String customDate;

    public String getIdfail() {
        return idfail;
    }

    public void setIdfail(String idfail) {
        this.idfail = idfail;
    }

    public String getKnd() {
        return knd;
    }

    public void setKnd(String knd) {
        this.knd = knd;
    }

    public String getDataDok() {
        return dataDok;
    }

    public void setDataDok(String dataDok) {
        this.dataDok = dataDok;
    }

    public String getPeriod() {
        return period;
    }

    public void setPeriod(String period) {
        this.period = period;
    }

    public String getOtchetGod() {
        return otchetGod;
    }

    public void setOtchetGod(String otchetGod) {
        this.otchetGod = otchetGod;
    }

    public String getKodNo() {
        return kodNo;
    }

    public void setKodNo(String kodNo) {
        this.kodNo = kodNo;
    }

    public String getNomKorr() {
        return nomKorr;
    }

    public void setNomKorr(String nomKorr) {
        this.nomKorr = nomKorr;
    }

    public String getPoMestu() {
        return poMestu;
    }

    public void setPoMestu(String poMestu) {
        this.poMestu = poMestu;
    }

    public String getOkved() {
        return okved;
    }

    public void setOkved(String okved) {
        this.okved = okved;
    }

    public String getTel() {
        return tel;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }

    public String getNaimOrg() {
        return naimOrg;
    }

    public void setNaimOrg(String naimOrg) {
        this.naimOrg = naimOrg;
    }

    public String getInnUl() {
        return innUl;
    }

    public void setInnUl(String innUl) {
        this.innUl = innUl;
    }

    public String getKpp() {
        return kpp;
    }

    public void setKpp(String kpp) {
        this.kpp = kpp;
    }

    public String getPrPodp() {
        return prPodp;
    }

    public void setPrPodp(String prPodp) {
        this.prPodp = prPodp;
    }

    public String getFamiliya() {
        return familiya;
    }

    public void setFamiliya(String familiya) {
        this.familiya = familiya;
    }

    public String getImya() {
        return imya;
    }

    public void setImya(String imya) {
        this.imya = imya;
    }

    public String getOtchestvo() {
        return otchestvo;
    }

    public void setOtchestvo(String otchestvo) {
        this.otchestvo = otchestvo;
    }

    public String getOktmo() {
        return oktmo;
    }

    public void setOktmo(String oktmo) {
        this.oktmo = oktmo;
    }

    public String getKbk() {
        return kbk;
    }

    public void setKbk(String kbk) {
        this.kbk = kbk;
    }

    public String getSumPu1731() {
        return sumPu1731;
    }

    public void setSumPu1731(String sumPu1731) {
        this.sumPu1731 = sumPu1731;
    }

    public String getNalPu164() {
        return nalPu164;
    }

    public void setNalPu164(String nalPu164) {
        this.nalPu164 = nalPu164;
    }

    public String getNalVosstObsch() {
        return nalVosstObsch;
    }

    public void setNalVosstObsch(String nalVosstObsch) {
        this.nalVosstObsch = nalVosstObsch;
    }

    public String getNalBaza() {
        return nalBaza;
    }

    public void setNalBaza(String nalBaza) {
        this.nalBaza = nalBaza;
    }

    public String getSumNal() {
        return sumNal;
    }

    public void setSumNal(String sumNal) {
        this.sumNal = sumNal;
    }

    public String getNaimKnPok() {
        return naimKnPok;
    }

    public void setNaimKnPok(String naimKnPok) {
        this.naimKnPok = naimKnPok;
    }

    public String getNaimKnProd() {
        return naimKnProd;
    }

    public void setNaimKnProd(String naimKnProd) {
        this.naimKnProd = naimKnProd;
    }

    public String getNaimZhuchVistSchF() {
        return naimZhuchVistSchF;
    }

    public void setNaimZhuchVistSchF(String naimZhuchVistSchF) {
        this.naimZhuchVistSchF = naimZhuchVistSchF;
    }

    public String getNaimZhuchPoluchSchF() {
        return naimZhuchPoluchSchF;
    }

    public void setNaimZhuchPoluchSchF(String naimZhuchPoluchSchF) {
        this.naimZhuchPoluchSchF = naimZhuchPoluchSchF;
    }

    public String getCustomDate() {
        return customDate;
    }

    public void setCustomDate(String customDate) {
        this.customDate = customDate;
    }
}


//<?xml version="1.0" encoding="windows-1251"?>
//<Файл xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" ИдФайл="NO_NDS_7834_7834_7816151039783450001_20150702_11370DB1-37F3-864B-B2CC-9950321AB005" ВерсПрог="Налогоплательщик ЮЛ 4.41.1" ВерсФорм="5.04" ПризнНал8-12="1" ПризнНал8="1" ПризнНал81="0" ПризнНал9="1" ПризнНал91="0" ПризнНал10="1" ПризнНал11="1" ПризнНал12="0">
//<Документ КНД="1151001" ДатаДок="27.02.2015" Период="21" ОтчетГод="2015" КодНО="7834" НомКорр="0" ПоМесту="213">
//<СвНП ОКВЭД="20.30.1" Тлф="(812)244-46-00">
//<НПЮЛ НаимОрг="Общество с ограниченной ответственностью &quot;СоюзБалтКомплект&quot;" ИННЮЛ="7816151039" КПП="783450001"/>
//</СвНП>
//<Подписант ПрПодп="1">
//<ФИО Фамилия="Кошицкий" Имя="Ярослав" Отчество="Иванович"/>
//</Подписант>
//<НДС>
//<СумУплНП ОКТМО="50015268" КБК="18210301000011000110" СумПУ_173.1="26022587"/>
//<СумУпл164 НалПУ164="26022587">
//<СумНалОб НалВосстОбщ="26022587">
//<РеалТов18 НалБаза="144569925" СумНал="26022587"/>
//</СумНалОб>
//</СумУпл164>
//<КнигаПокуп НаимКнПок="NO_NDS.8_7834_7834_7816151039783450001_20150702_E9BC2373-B1B6-C345-A166-CA1BCE8A38AE.xml"/>
//<КнигаПрод НаимКнПрод="NO_NDS.9_7834_7834_7816151039783450001_20150702_39E15714-451C-8743-99A9-EA5EDF79A318.xml"/>
//<ЖУчВыстСчФ НаимЖУчВыстСчФ="NO_NDS.10_7834_7834_7816151039783450001_20150702_58900656-F8B1-3D4A-8AD4-1FF3D000E495.xml"/>
//<ЖУчПолучСчФ НаимЖУчПолучСчФ="NO_NDS.11_7834_7834_7816151039783450001_20150702_A177FF0D-2408-1B4C-B899-883C6BC23171.xml"/>
//</НДС>
//</Документ>
//</Файл>