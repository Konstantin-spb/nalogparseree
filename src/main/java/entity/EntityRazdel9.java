package entity;

//<?xml version="1.0" encoding="windows-1251"?>
//<Файл xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" ИдФайл="NO_NDS.9_7834_7834_7816151039783450001_20150313_D86DEE7F-130A-2A4A-8CC7-D33F81DE1162" ВерсПрог="Налогоплательщик ЮЛ 4.40.4" ВерсФорм="5.04">
// <Документ Индекс="0000090" НомКорр="0">
// <КнигаПрод>
// <КнПродСтр НомерПор="111" НомСчФПрод="22222" ДатаСчФПрод="01.01.2015" НомИспрСчФ="333"
// ДатаИспрСчФ="02.01.2015" НомКСчФПрод="4444" ДатаКСчФПрод="03.01.2015" НомИспрКСчФ="555"
// ДатаИспрКСчФ="04.01.2015" ОКВ="643" СтоимПродСФВ="999.90" СтоимПродСФ="111.10"
// СтоимПродСФ18="2.20" СумНДССФ18="44.00" СтоимПродОсв="55.00">

// <КодВидОпер>01</КодВидОпер>
// <ДокПдтвОпл НомДокПдтвОпл="123" ДатаДокПдтвОпл="05.01.2015"/>
// <СвПокуп>
// <СведЮЛ ИННЮЛ="0560026347" КПП="056001001"/>
// </СвПокуп>
// <СвПос>
// <СведЮЛ ИННЮЛ="5036039177" КПП="503601001"/>
// </СвПос>
// </КнПродСтр>

// </КнигаПрод>
// </Документ>
// </Файл>

public class EntityRazdel9 {

    String nomerPor;    String nomSchFProd;    String dataSchFProd;    String nomIsprSchF;    String dataIcprSchF;    String nomKSchFProd;    String dataKSchFProd;
    String nomIsprKSchF;    String dataIsprKschF;    String okv = "643";    String stoimProdSFV;    String stoimProdSF;    String stoimProdSF18_10; String stoimProdSF0;
    String sumNDSSF;    String stoimProdOsv;    String kodVidOper;    String nomDokPdtvOpl;    String dataDokPdtvOpl;
    String innPokup;
    String kppPokup;
    String innPosrednik;
    String kppPosrednik;

    public String getStoimProdSF0() {
        return stoimProdSF0;
    }

    public void setStoimProdSF0(String stoimProdSF0) {
        this.stoimProdSF0 = stoimProdSF0;
    }

    public String getNomerPor() {
        return nomerPor;
    }

    public void setNomerPor(String nomerPor) {
        this.nomerPor = nomerPor;
    }

    public String getNomSchFProd() {
        return nomSchFProd;
    }

    public void setNomSchFProd(String nomSchFProd) {
        this.nomSchFProd = nomSchFProd;
    }

    public String getDataSchFProd() {
        return dataSchFProd;
    }

    public void setDataSchFProd(String dataSchFProd) {
        this.dataSchFProd = dataSchFProd;
    }

    public String getNomIsprSchF() {
        return nomIsprSchF;
    }

    public void setNomIsprSchF(String nomIsprSchF) {
        this.nomIsprSchF = nomIsprSchF;
    }

    public String getDataIcprSchF() {
        return dataIcprSchF;
    }

    public void setDataIcprSchF(String dataIcprSchF) {
        this.dataIcprSchF = dataIcprSchF;
    }

    public String getNomKSchFProd() {
        return nomKSchFProd;
    }

    public void setNomKSchFProd(String nomKSchFProd) {
        this.nomKSchFProd = nomKSchFProd;
    }

    public String getDataKSchFProd() {
        return dataKSchFProd;
    }

    public void setDataKSchFProd(String dataKSchFProd) {
        this.dataKSchFProd = dataKSchFProd;
    }

    public String getNomIsprKSchF() {
        return nomIsprKSchF;
    }

    public void setNomIsprKSchF(String nomIsprKSchF) {
        this.nomIsprKSchF = nomIsprKSchF;
    }

    public String getDataIsprKschF() {
        return dataIsprKschF;
    }

    public void setDataIsprKschF(String dataIsprKschF) {
        this.dataIsprKschF = dataIsprKschF;
    }

    public String getOkv() {
        return okv;
    }

    public void setOkv(String okv) {
        this.okv = okv;
    }

    public String getStoimProdSFV() {
        return stoimProdSFV;
    }

    public void setStoimProdSFV(String stoimProdSFV) {
        this.stoimProdSFV = stoimProdSFV;
    }

    public String getStoimProdSF() {
        return stoimProdSF;
    }

    public void setStoimProdSF(String stoimProdSF) {
        this.stoimProdSF = stoimProdSF;
    }

    public String getStoimProdSF18_10() {
        return stoimProdSF18_10;
    }

    public void setStoimProdSF18_10(String stoimProdSF18_10) {
        this.stoimProdSF18_10 = stoimProdSF18_10;
    }

    public String getSumNDSSF() {
        return sumNDSSF;
    }

    public void setSumNDSSF(String sumNDSSF) {
        this.sumNDSSF = sumNDSSF;
    }

    public String getStoimProdOsv() {
        return stoimProdOsv;
    }

    public void setStoimProdOsv(String stoimProdOsv) {
        this.stoimProdOsv = stoimProdOsv;
    }

    public String getKodVidOper() {
        return kodVidOper;
    }

    public void setKodVidOper(String kodVidOper) {
        this.kodVidOper = kodVidOper;
    }

    public String getNomDokPdtvOpl() {
        return nomDokPdtvOpl;
    }

    public void setNomDokPdtvOpl(String nomDokPdtvOpl) {
        this.nomDokPdtvOpl = nomDokPdtvOpl;
    }

    public String getDataDokPdtvOpl() {
        return dataDokPdtvOpl;
    }

    public void setDataDokPdtvOpl(String dataDokPdtvOpl) {
        this.dataDokPdtvOpl = dataDokPdtvOpl;
    }

    public String getInnPokup() {
        return innPokup;
    }

    public void setInnPokup(String innPokup) {
        this.innPokup = innPokup;
    }

    public String getKppPokup() {
        return kppPokup;
    }

    public void setKppPokup(String kppPokup) {
        this.kppPokup = kppPokup;
    }

    public String getInnPosrednik() {
        return innPosrednik;
    }

    public void setInnPosrednik(String innPosrednik) {
        this.innPosrednik = innPosrednik;
    }

    public String getKppPosrednik() {
        return kppPosrednik;
    }

    public void setKppPosrednik(String kppPosrednik) {
        this.kppPosrednik = kppPosrednik;
    }
}
