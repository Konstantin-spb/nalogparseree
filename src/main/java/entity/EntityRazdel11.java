package entity;

/**
 * Created by konstantin on 24.03.15.
 */
public class EntityRazdel11 {

//    <?xml version="1.0" encoding="windows-1251" ?>
//            <Файл xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" ИдФайл="NO_NDS.11_7834_7834_7816151039783450001_20150313_0F6BE0D8-180B-E846-9B5A-C538A7AD600B" ВерсПрог="Налогоплательщик ЮЛ 4.40.4" ВерсФорм="5.04">
//            <Документ Индекс="0000110" НомКорр="0">

//   <ЖУчПолучСчФ>
//    <ЖУчПолучСчФСтр НомерПор="1111" ДатаПолуч="07.03.2015" НомСчФПрод="2222" ДатаСчФПрод="02.03.2015" НомИспрСчФ="333"
//      ДатаИспрСчФ="03.03.2015" НомКСчФПрод="4444" ДатаКСчФПрод="05.03.2015" НомИспрКСчФ="555" ДатаИспрКСчФ="06.03.2015"
//      КодВидСд="1" ОКВ="643" СтоимТовСчФВс="0.00" РазСтКСчФУм="122.00" РазСтКСчФУв="133.00" РазНДСКСчФУм="144" РазНДСКСчФУв="155">
//    <КодВидОпер>04</КодВидОпер>
//            <СвПрод>
//    <СведЮЛ ИННЮЛ="3665078588" КПП="366501001" />
//    </СвПрод>
//            <СвКомис>
//    <СведЮЛ ИННЮЛ="3202009019" КПП="324501001" />
//    </СвКомис>
//    </ЖУчПолучСчФСтр>

//    <ЖУчПолучСчФСтр НомерПор="1" ДатаПолуч="03.03.2015" НомСчФПрод="123" ДатаСчФПрод="02.03.2015" КодВидСд="1" СтоимТовСчФВс="10000.00" СумНДССчФ="1800">
//    <КодВидОпер>01</КодВидОпер>
//    <СвПрод>
//    <СведИП ИННФЛ="583600321567" />
//    </СвПрод>
//    </ЖУчПолучСчФСтр>

//<ЖУчПолучСчФСтр НомерПор="148"
// РазСтКСчФУм="12"
// РазСтКСчФУв="3"
// РазНДСКСчФУм="4"
// РазНДСКСчФУв="5">

    String dataPoluch;
    String nomSchFProd;
    String dataSchFProd;
    String kodVidSd;
    String okv;
    String stoimTovSchFVs = "0";
    String sumNDSSchF;
    String kodVidOper;
    String innProd;
    String kppProd;
    String innKomis;
    String kppKomis;

    String nomIsprSchF;
    String dataIsprSchF;
    String nomKSchFProd;
    String dataKSchFProd;
    String nomIsprKSchF;
    String dataIsprKSchF;
    String razStKSchFUm;
    String razStKSchFUv;
    String razNDSKSchFUm;
    String razNDSKSchFUv;

    public String getDataPoluch() {
        return dataPoluch;
    }

    public void setDataPoluch(String dataPoluch) {
        this.dataPoluch = dataPoluch;
    }

    public String getNomSchFProd() {
        return nomSchFProd;
    }

    public void setNomSchFProd(String nomSchFProd) {
        this.nomSchFProd = nomSchFProd;
    }

    public String getDataSchFProd() {
        return dataSchFProd;
    }

    public void setDataSchFProd(String dataSchFProd) {
        this.dataSchFProd = dataSchFProd;
    }

    public String getKodVidSd() {
        return kodVidSd;
    }

    public void setKodVidSd(String kodVidSd) {
        this.kodVidSd = kodVidSd;
    }

    public String getOkv() {
        return okv;
    }

    public void setOkv(String okv) {
        this.okv = okv;
    }

    public String getStoimTovSchFVs() {
        return stoimTovSchFVs;
    }

    public void setStoimTovSchFVs(String stoimTovSchFVs) {
        this.stoimTovSchFVs = stoimTovSchFVs;
    }

    public String getSumNDSSchF() {
        return sumNDSSchF;
    }

    public void setSumNDSSchF(String sumNDSSchF) {
        this.sumNDSSchF = sumNDSSchF;
    }

    public String getKodVidOper() {
        return kodVidOper;
    }

    public void setKodVidOper(String kodVidOper) {
        this.kodVidOper = kodVidOper;
    }

    public String getInnProd() {
        return innProd;
    }

    public void setInnProd(String innProd) {
        this.innProd = innProd;
    }

    public String getKppProd() {
        return kppProd;
    }

    public void setKppProd(String kppProd) {
        this.kppProd = kppProd;
    }

    public String getInnKomis() {
        return innKomis;
    }

    public void setInnKomis(String innKomis) {
        this.innKomis = innKomis;
    }

    public String getKppKomis() {
        return kppKomis;
    }

    public void setKppKomis(String kppKomis) {
        this.kppKomis = kppKomis;
    }

    public String getNomIsprSchF() {
        return nomIsprSchF;
    }

    public void setNomIsprSchF(String nomIsprSchF) {
        this.nomIsprSchF = nomIsprSchF;
    }

    public String getDataIsprSchF() {
        return dataIsprSchF;
    }

    public void setDataIsprSchF(String dataIsprSchF) {
        this.dataIsprSchF = dataIsprSchF;
    }

    public String getNomKSchFProd() {
        return nomKSchFProd;
    }

    public void setNomKSchFProd(String nomKSchFProd) {
        this.nomKSchFProd = nomKSchFProd;
    }

    public String getDataKSchFProd() {
        return dataKSchFProd;
    }

    public void setDataKSchFProd(String dataKSchFProd) {
        this.dataKSchFProd = dataKSchFProd;
    }

    public String getNomIsprKSchF() {
        return nomIsprKSchF;
    }

    public void setNomIsprKSchF(String nomIsprKSchF) {
        this.nomIsprKSchF = nomIsprKSchF;
    }

    public String getDataIsprKSchF() {
        return dataIsprKSchF;
    }

    public void setDataIsprKSchF(String dataIsprKSchF) {
        this.dataIsprKSchF = dataIsprKSchF;
    }

    public String getRazStKSchFUm() {
        return razStKSchFUm;
    }

    public void setRazStKSchFUm(String razStKSchFUm) {
        this.razStKSchFUm = razStKSchFUm;
    }

    public String getRazStKSchFUv() {
        return razStKSchFUv;
    }

    public void setRazStKSchFUv(String razStKSchFUv) {
        this.razStKSchFUv = razStKSchFUv;
    }

    public String getRazNDSKSchFUm() {
        return razNDSKSchFUm;
    }

    public void setRazNDSKSchFUm(String razNDSKSchFUm) {
        this.razNDSKSchFUm = razNDSKSchFUm;
    }

    public String getRazNDSKSchFUv() {
        return razNDSKSchFUv;
    }

    public void setRazNDSKSchFUv(String razNDSKSchFUv) {
        this.razNDSKSchFUv = razNDSKSchFUv;
    }
}
