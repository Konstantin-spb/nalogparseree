package entity.subEntities;

/**
 * Created by konstantin on 18.03.15.
 */
public class SubEntities10Prod {

//    СтоимТовСчФВс="1000.00" СумНДССчФ="180"
    String innProd;
    String kppProd;
    String nomSchFOtProdProd;
    String dataSchFOtProdProd;
    String okvProd;
    String stoimTovSchFVs;
    String sumNDSSchF;
    String razStKSchFUmProd;
    String razStKSchAUvProd;
    String razNDSKSchFUmProd;
    String razNDSKSchFUvProd;

    public String getStoimTovSchFVs() {
        return stoimTovSchFVs;
    }

    public void setStoimTovSchFVs(String stoimTovSchFVs) {
        this.stoimTovSchFVs = stoimTovSchFVs;
    }

    public String getSumNDSSchF() {
        return sumNDSSchF;
    }

    public void setSumNDSSchF(String sumNDSSchF) {
        this.sumNDSSchF = sumNDSSchF;
    }

    public String getInnProd() {
        return innProd;
    }

    public void setInnProd(String innProd) {
        this.innProd = innProd;
    }

    public String getKppProd() {
        return kppProd;
    }

    public void setKppProd(String kppProd) {
        this.kppProd = kppProd;
    }

    public String getNomSchFOtProdProd() {
        return nomSchFOtProdProd;
    }

    public void setNomSchFOtProdProd(String nomSchFOtProdProd) {
        this.nomSchFOtProdProd = nomSchFOtProdProd;
    }

    public String getDataSchFOtProdProd() {
        return dataSchFOtProdProd;
    }

    public void setDataSchFOtProdProd(String dataSchFOtProdProd) {
        this.dataSchFOtProdProd = dataSchFOtProdProd;
    }

    public String getOkvProd() {
        return okvProd;
    }

    public void setOkvProd(String okvProd) {
        this.okvProd = okvProd;
    }

    public String getRazStKSchFUmProd() {
        return razStKSchFUmProd;
    }

    public void setRazStKSchFUmProd(String razStKSchFUmProd) {
        this.razStKSchFUmProd = razStKSchFUmProd;
    }

    public String getRazStKSchAUvProd() {
        return razStKSchAUvProd;
    }

    public void setRazStKSchAUvProd(String razStKSchAUvProd) {
        this.razStKSchAUvProd = razStKSchAUvProd;
    }

    public String getRazNDSKSchFUmProd() {
        return razNDSKSchFUmProd;
    }

    public void setRazNDSKSchFUmProd(String razNDSKSchFUmProd) {
        this.razNDSKSchFUmProd = razNDSKSchFUmProd;
    }

    public String getRazNDSKSchFUvProd() {
        return razNDSKSchFUvProd;
    }

    public void setRazNDSKSchFUvProd(String razNDSKSchFUvProd) {
        this.razNDSKSchFUvProd = razNDSKSchFUvProd;
    }
}
