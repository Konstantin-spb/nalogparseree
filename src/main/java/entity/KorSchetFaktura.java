package entity;

/**
 * Created by konstantin on 26.02.15.
 */
public class KorSchetFaktura {

    public KorSchetFaktura() {
        this.svProdInnUl = this.svOEDOtprInnUl;
        this.svProdKpp = this.svOEDOtprIdEDO;
        this.kolTovPosle = this.kolTovDo;
    }

//    <Файл ВерсПрог="Custom" ИдФайл="ON_KORSFAKT_7834_7834_20150224_c6e5c190-bc27-11e4-b7ba-448a5b580838" ВерсФорм="5.01" xsi:noNamespaceSchemaLocation="ON_KORSFAKT_1_911_01_05_01_03.xsd" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
    String fileId;
//    <СвУчДокОбор ИдПок="ИдПокупателя" ИдОтпр="ИдОтправителя">
    String IdPok;
    String IdOtp;
//    <СвОЭДОтпр ИННЮЛ="7816151039" НаимОрг="СоюзБалтКомплект" ИдЭДО="000"/>
    String svOEDOtprInnUl = "7816151039";
    String svOEDOtprNaimOrg = "СоюзБалтКомплект";
    String svOEDOtprIdEDO = "783450001";
//    </СвУчДокОбор>
//    <Документ КНД="1115108">
//    <СвКСчФ НомерСчФ="a" ДатаКСчФ="31.12.2000" НомерКСчФ="a" КодОКВ="000" ДатаСчФ="31.12.2000">
    String nomerSchF;
    String dataKSchF = "";
    String nomerKSchF = "";
    String kodOKV = "643";
    String dataSchF;
//    String dataIsprSchF;//ДатаИспрСчФ
//    <КодВидОпер>01</КодВидОпер>
    String kodVidOper;
//    <ИспрКСчФ НомИспрКСчФ="a" ДатаИспрКСчФ="31.12.2000"/>
    String nomIsprKSchF;
    String dataIsprKSchF;
//    <ИспрСчФ НомИспрСчФ="a" ДатаИспрСчФ="31.12.2000"/>
    String nomIsprSchF;
    String dataIsprSchF;
//    <СвПрод>
//    <ИдСв>
//    <СвЮЛ ИННЮЛ="7816151039" НаимОрг="СоюзБалтКомплект" КПП="783450001"/>
    String svProdInnUl;
    String svProdNaimOrg;
    String svProdKpp;
//    </ИдСв>
//    <Адрес>
//    <АдрРФ КодРегион="00"/>
//    </Адрес>
//    </СвПрод>

//    <СвПокуп>
//    <ИдСв>
//    <СвЮЛ ИННЮЛ="2801197391" НаимОрг="ООО МЕБЕЛЬ-КОМПЛЕКТ" КПП="280101001"/>
    String svPokupInnUl;
    String svPokupNaimOrg;
    String svPokupKpp;
//    </ИдСв>
//    <Адрес>
//    <АдрРФ КодРегион="00"/>
//    </Адрес>
//    </СвПокуп>
//    </СвКСчФ>



    //    <ТаблКСчФ>
//    <СведТов ЦенаТовДо="123456789123456.12345678912" НомСтр="123456" КолТовДо="123456789123456.123" НаимТов="a" ЦенаТовПосле="123456789123456.12345678912" КолТовПосле="123456789123456.123">

    float tsenaTovaraDoBezNDS;
    float tsenaTovaraDoSNDS;
    float tsenaTovaraPosleBezNDS;
    float tsenaTovaraPosleSNDS;

    float raznicaSummNDS;
    float raznicaStoimostVkluchNDS;

    float tsenaTovDo = 100f;
    String nomStr;
    String kolTovDo = "1";
    String naimTov = "Товар";
    float tsenaTovPosle;
    String kolTovPosle;
//    <СтТовБезНДС СтоимДоИзм="123456789123456.12" СтоимУм="123456789123456.12" СтоимПослеИзм="123456789123456.12" СтоимУвел="123456789123456.12"/>
    String stTovBezNDSStoimDoIzm;
    String stTovBezNDSStoimUm;
    String stTovBezNDSStoimPosleIzm;
    String stTovBezNDSStoimUvel;
//    <АкцизДо СумАкциз="0000"/>
//    <АкцизПосле СумАкциз="0000"/>
//    <АкцизРазн>
//    <СумУвел>123456789123456.12</СумУвел>
    String akcizRaznSumUvel;
//    </АкцизРазн>
//    <НалСтДо НалСтВел="без НДС" НалСтТип="текст"/>
    String nalStDoNalStVel = "18";
    String nalStDoNalStTip = "процент";
//    <НалСтПосле НалСтВел="10/110" НалСтТип="текст"/>
    String nalStPosleNalStVel = "18";
    String nalStPosleNalStTip = "процент";
//    <СумНалДо СумНДС="0"/>
    float sumNalDoSumNDS = 18;
//    <СумНалПосле СумНДС="0"/>
    float sumNalPosleSumNDS;
//    <СумНалРазн>
//    <СумУвел>123456789123456.12</СумУвел>
    String sumNalRaznSumUvel;
//    </СумНалРазн>
//    <СтТовУчНал СтоимДоИзм="123456789123456.12" СтоимУм="123456789123456.12" СтоимПослеИзм="123456789123456.12" СтоимУвел="123456789123456.12"/>
    String stTovUchNalStoimDoIzm = "100";
    String stTovUchNalStoimUm;
    float stTovUchNalStoimPosleIzm;
    float stTovUchNalStoimUvel;
//    </СведТов>
//    </ТаблКСчФ>

//    <Подписант>
//    <ЮЛ ИННЮЛ="100000000000">
//    <ФИО Фамилия="a" Отчество="a" Имя="a"/>
//    </ИП>
//    </Подписант>
//    </Документ>
    String fio = "Фамилия=\"Иванов\" Имя=\"Петр\" Отчество=\"Сергеевич\"";

    public String getFileId() {
        return fileId;
    }

    public void setFileId(String fileId) {
        this.fileId = fileId;
    }

    public String getIdPok() {
        return IdPok;
    }

    public void setIdPok(String idPok) {
        IdPok = idPok;
    }

    public String getIdOtp() {
        return IdOtp;
    }

    public void setIdOtp(String idOtp) {
        IdOtp = idOtp;
    }

    public String getSvOEDOtprInnUl() {
        return svOEDOtprInnUl;
    }

    public void setSvOEDOtprInnUl(String svOEDOtprInnUl) {
        this.svOEDOtprInnUl = svOEDOtprInnUl;
    }

    public String getSvOEDOtprNaimOrg() {
        return svOEDOtprNaimOrg;
    }

    public void setSvOEDOtprNaimOrg(String svOEDOtprNaimOrg) {
        this.svOEDOtprNaimOrg = svOEDOtprNaimOrg;
    }

    public String getSvOEDOtprIdEDO() {
        return svOEDOtprIdEDO;
    }

    public void setSvOEDOtprIdEDO(String svOEDOtprIdEDO) {
        this.svOEDOtprIdEDO = svOEDOtprIdEDO;
    }

    public String getNomerSchF() {
        return nomerSchF;
    }

    public void setNomerSchF(String nomerSchF) {
        this.nomerSchF = nomerSchF;
    }

    public String getDataKSchF() {
        return dataKSchF;
    }

    public void setDataKSchF(String dataKSchF) {
        this.dataKSchF = dataKSchF;
    }

    public String getNomerKSchF() {
        return nomerKSchF;
    }

    public void setNomerKSchF(String nomerKSchF) {
        this.nomerKSchF = nomerKSchF;
    }

    public String getKodOKV() {
        return kodOKV;
    }

    public void setKodOKV(String kodOKV) {
        this.kodOKV = kodOKV;
    }

    public String getDataSchF() {
        return dataSchF;
    }

    public void setDataSchF(String dataSchF) {
        this.dataSchF = dataSchF;
    }

//    public String getIsprSchF() {
//        return isprSchF;
//    }
//
//    public void setIsprSchF(String isprSchF) {
//        this.isprSchF = isprSchF;
//    }

    public String getKodVidOper() {
        return kodVidOper;
    }

    public void setKodVidOper(String kodVidOper) {
        this.kodVidOper = kodVidOper;
    }

    public String getNomIsprKSchF() {
        return nomIsprKSchF;
    }

    public void setNomIsprKSchF(String nomIsprKSchF) {
        this.nomIsprKSchF = nomIsprKSchF;
    }

    public String getDataIsprKSchF() {
        return dataIsprKSchF;
    }

    public void setDataIsprKSchF(String dataIsprKSchF) {
        this.dataIsprKSchF = dataIsprKSchF;
    }

    public String getNomIsprSchF() {
        return nomIsprSchF;
    }

    public void setNomIsprSchF(String nomIsprSchF) {
        this.nomIsprSchF = nomIsprSchF;
    }

    public String getDataIsprSchF() {
        return dataIsprSchF;
    }

    public void setDataIsprSchF(String dataIsprSchF) {
        this.dataIsprSchF = dataIsprSchF;
    }

    public String getSvProdInnUl() {
        return svProdInnUl;
    }

    public void setSvProdInnUl(String svProdInnUl) {
        this.svProdInnUl = svProdInnUl;
    }

    public String getSvProdNaimOrg() {
        return svProdNaimOrg;
    }

    public void setSvProdNaimOrg(String svProdNaimOrg) {
        this.svProdNaimOrg = svProdNaimOrg;
    }

    public String getSvProdKpp() {
        return svProdKpp;
    }

    public void setSvProdKpp(String svProdKpp) {
        this.svProdKpp = svProdKpp;
    }

    public String getSvPokupInnUl() {
        return svPokupInnUl;
    }

    public void setSvPokupInnUl(String svPokupInnUl) {
        this.svPokupInnUl = svPokupInnUl;
    }

    public String getSvPokupNaimOrg() {
        return svPokupNaimOrg;
    }

    public void setSvPokupNaimOrg(String svPokupNaimOrg) {
        this.svPokupNaimOrg = svPokupNaimOrg;
    }

    public String getSvPokupKpp() {
        return svPokupKpp;
    }

    public void setSvPokupKpp(String svPokupKpp) {
        this.svPokupKpp = svPokupKpp;
    }

    public float getTsenaTovaraDoBezNDS() {
        return tsenaTovaraDoBezNDS;
    }

    public void setTsenaTovaraDoBezNDS(float tsenaTovaraDoBezNDS) {
        this.tsenaTovaraDoBezNDS = tsenaTovaraDoBezNDS;
    }

    public float getTsenaTovaraDoSNDS() {
        return tsenaTovaraDoSNDS;
    }

    public void setTsenaTovaraDoSNDS(float tsenaTovaraDoSNDS) {
        this.tsenaTovaraDoSNDS = tsenaTovaraDoSNDS;
    }

    public float getTsenaTovaraPosleBezNDS() {
        return tsenaTovaraPosleBezNDS;
    }

    public void setTsenaTovaraPosleBezNDS(float tsenaTovaraPosleBezNDS) {
        this.tsenaTovaraPosleBezNDS = tsenaTovaraPosleBezNDS;
    }

    public float getTsenaTovaraPosleSNDS() {
        return tsenaTovaraPosleSNDS;
    }

    public void setTsenaTovaraPosleSNDS(float tsenaTovaraPosleSNDS) {
        this.tsenaTovaraPosleSNDS = tsenaTovaraPosleSNDS;
    }

    public float getRaznicaSummNDS() {
        return raznicaSummNDS;
    }

    public void setRaznicaSummNDS(float raznicaSummNDS) {
        this.raznicaSummNDS = raznicaSummNDS;
    }

    public float getRaznicaStoimostVkluchNDS() {
        return raznicaStoimostVkluchNDS;
    }

    public void setRaznicaStoimostVkluchNDS(float raznicaStoimostVkluchNDS) {
        this.raznicaStoimostVkluchNDS = raznicaStoimostVkluchNDS;
    }

    public float getTsenaTovDo() {
        return tsenaTovDo;
    }

    public void setTsenaTovDo(float tsenaTovDo) {
        this.tsenaTovDo = tsenaTovDo;
    }

    public String getNomStr() {
        return nomStr;
    }

    public void setNomStr(String nomStr) {
        this.nomStr = nomStr;
    }

    public String getKolTovDo() {
        return kolTovDo;
    }

    public void setKolTovDo(String kolTovDo) {
        this.kolTovDo = kolTovDo;
    }

    public String getNaimTov() {
        return naimTov;
    }

    public void setNaimTov(String naimTov) {
        this.naimTov = naimTov;
    }

    public float getTsenaTovPosle() {
        return tsenaTovPosle;
    }

    public void setTsenaTovPosle(float tsenaTovPosle) {
        this.tsenaTovPosle = tsenaTovPosle;
    }

    public String getKolTovPosle() {
        return kolTovPosle;
    }

    public void setKolTovPosle(String kolTovPosle) {
        this.kolTovPosle = kolTovPosle;
    }

    public String getStTovBezNDSStoimDoIzm() {
        return stTovBezNDSStoimDoIzm;
    }

    public void setStTovBezNDSStoimDoIzm(String stTovBezNDSStoimDoIzm) {
        this.stTovBezNDSStoimDoIzm = stTovBezNDSStoimDoIzm;
    }

    public String getStTovBezNDSStoimUm() {
        return stTovBezNDSStoimUm;
    }

    public void setStTovBezNDSStoimUm(String stTovBezNDSStoimUm) {
        this.stTovBezNDSStoimUm = stTovBezNDSStoimUm;
    }

    public String getStTovBezNDSStoimPosleIzm() {
        return stTovBezNDSStoimPosleIzm;
    }

    public void setStTovBezNDSStoimPosleIzm(String stTovBezNDSStoimPosleIzm) {
        this.stTovBezNDSStoimPosleIzm = stTovBezNDSStoimPosleIzm;
    }

    public String getStTovBezNDSStoimUvel() {
        return stTovBezNDSStoimUvel;
    }

    public void setStTovBezNDSStoimUvel(String stTovBezNDSStoimUvel) {
        this.stTovBezNDSStoimUvel = stTovBezNDSStoimUvel;
    }

    public String getAkcizRaznSumUvel() {
        return akcizRaznSumUvel;
    }

    public void setAkcizRaznSumUvel(String akcizRaznSumUvel) {
        this.akcizRaznSumUvel = akcizRaznSumUvel;
    }

    public String getNalStDoNalStVel() {
        return nalStDoNalStVel;
    }

    public void setNalStDoNalStVel(String nalStDoNalStVel) {
        this.nalStDoNalStVel = nalStDoNalStVel;
    }

    public String getNalStDoNalStTip() {
        return nalStDoNalStTip;
    }

    public void setNalStDoNalStTip(String nalStDoNalStTip) {
        this.nalStDoNalStTip = nalStDoNalStTip;
    }

    public String getNalStPosleNalStVel() {
        return nalStPosleNalStVel;
    }

    public void setNalStPosleNalStVel(String nalStPosleNalStVel) {
        this.nalStPosleNalStVel = nalStPosleNalStVel;
    }

    public String getNalStPosleNalStTip() {
        return nalStPosleNalStTip;
    }

    public void setNalStPosleNalStTip(String nalStPosleNalStTip) {
        this.nalStPosleNalStTip = nalStPosleNalStTip;
    }

    public float getSumNalDoSumNDS() {
        return sumNalDoSumNDS;
    }

    public void setSumNalDoSumNDS(float sumNalDoSumNDS) {
        this.sumNalDoSumNDS = sumNalDoSumNDS;
    }

    public float getSumNalPosleSumNDS() {
        return sumNalPosleSumNDS;
    }

    public void setSumNalPosleSumNDS(float sumNalPosleSumNDS) {
        this.sumNalPosleSumNDS = sumNalPosleSumNDS;
    }

    public String getSumNalRaznSumUvel() {
        return sumNalRaznSumUvel;
    }

    public void setSumNalRaznSumUvel(String sumNalRaznSumUvel) {
        this.sumNalRaznSumUvel = sumNalRaznSumUvel;
    }

    public String getStTovUchNalStoimDoIzm() {
        return stTovUchNalStoimDoIzm;
    }

    public void setStTovUchNalStoimDoIzm(String stTovUchNalStoimDoIzm) {
        this.stTovUchNalStoimDoIzm = stTovUchNalStoimDoIzm;
    }

    public String getStTovUchNalStoimUm() {
        return stTovUchNalStoimUm;
    }

    public void setStTovUchNalStoimUm(String stTovUchNalStoimUm) {
        this.stTovUchNalStoimUm = stTovUchNalStoimUm;
    }

    public float getStTovUchNalStoimPosleIzm() {
        return stTovUchNalStoimPosleIzm;
    }

    public void setStTovUchNalStoimPosleIzm(float stTovUchNalStoimPosleIzm) {
        this.stTovUchNalStoimPosleIzm = stTovUchNalStoimPosleIzm;
    }

    public float getStTovUchNalStoimUvel() {
        return stTovUchNalStoimUvel;
    }

    public void setStTovUchNalStoimUvel(float stTovUchNalStoimUvel) {
        this.stTovUchNalStoimUvel = stTovUchNalStoimUvel;
    }

    public String getFio() {
        return fio;
    }

    public void setFio(String fio) {
        this.fio = fio;
    }
}
