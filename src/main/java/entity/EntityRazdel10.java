package entity;

import entity.subEntities.SubEntities10Prod;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by konstantin on 18.03.15.
 */
public class EntityRazdel10 {

//    <?xml version="1.0" encoding="windows-1251" ?>
//            - <Файл xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" ИдФайл="NO_NDS.10_7834_7834_7816151039783450001_20150318_C702459C-658A-5D49-9B4C-6AEE32966D9B" ВерсПрог="Налогоплательщик ЮЛ 4.40.4" ВерсФорм="5.04">
//            - <Документ Индекс="0000100" НомКорр="0">
//            - <ЖУчВыстСчФ>

//    <ЖУчВыстСчФСтр НомерПор="1" ДатаВыст="10.03.2015" НомСчФПрод="123" ДатаСчФПрод="02.03.2015" НомИспрСчФ="23" ДатаИспрСчФ="04.03.2015"
// НомКСчФПрод="234" ДатаКСчФПрод="11.03.2015" НомИспрКСчФ="34" ДатаИспрКСчФ="17.03.2015">
//    <КодВидОпер>01</КодВидОпер>
//            - <СвПокуп>
//    <СведЮЛ ИННЮЛ="2801092430" КПП="280101001" />
//    </СвПокуп>
//            - <СвПосрДеят НомСчФОтПрод="987" ДатаСчФОтПрод="02.03.2015" ОКВ="643" СтоимТовСчФВс="1000.00" СумНДССчФ="180" РазСтКСчФУм="100.00" РазСтКСчФУв="120.00" РазНДСКСчФУм="130" РазНДСКСчФУв="140">
//            - <СвПрод>
//    <СведЮЛ ИННЮЛ="5021013190" КПП="502101001" />
//    </СвПрод>

//    </СвПосрДеят>
//            - <СвПосрДеят НомСчФОтПрод="765" ДатаСчФОтПрод="04.03.2015" ОКВ="643" РазСтКСчФУм="200.00" РазСтКСчФУв="210.00" РазНДСКСчФУм="220" РазНДСКСчФУв="230">
//            - <СвПрод>
//    <СведЮЛ ИННЮЛ="5042063636" КПП="504201001" />
//    </СвПрод>
//    </СвПосрДеят>
//    </ЖУчВыстСчФСтр>


    public EntityRazdel10() {
        prods = new ArrayList<SubEntities10Prod>();
    }

    List<SubEntities10Prod> prods;

    String nomerPor;
    String dataVyst;
    String nomSchFPokup;
    String dataSchFPokup;
    String nomIsprSchF;
    String dataIsprSchF;
    String nomKSchFPokup;
    String dataKSchDPokup;
    String nomIsprKSchF;
    String dataIsprKSchf;
    String kodVidOper;
    String innPokup;
    String kppPokup;
    String okvPokup;
    String razStKSchFUmPokup;
    String razStKSchAUvPokup;
    String razNDSKSchFUmPokup;
    String razNDSKSchFUvPokup;

    public List<SubEntities10Prod> getProds() {
        return prods;
    }

    public void setProds(List<SubEntities10Prod> prods) {
        this.prods = prods;
    }

    public String getNomerPor() {
        return nomerPor;
    }

    public void setNomerPor(String nomerPor) {
        this.nomerPor = nomerPor;
    }

    public String getDataVyst() {
        return dataVyst;
    }

    public void setDataVyst(String dataVyst) {
        this.dataVyst = dataVyst;
    }

    public String getNomSchFPokup() {
        return nomSchFPokup;
    }

    public void setNomSchFPokup(String nomSchFPokup) {
        this.nomSchFPokup = nomSchFPokup;
    }

    public String getDataSchFPokup() {
        return dataSchFPokup;
    }

    public void setDataSchFPokup(String dataSchFPokup) {
        this.dataSchFPokup = dataSchFPokup;
    }

    public String getNomIsprSchF() {
        return nomIsprSchF;
    }

    public void setNomIsprSchF(String nomIsprSchF) {
        this.nomIsprSchF = nomIsprSchF;
    }

    public String getDataIsprSchF() {
        return dataIsprSchF;
    }

    public void setDataIsprSchF(String dataIsprSchF) {
        this.dataIsprSchF = dataIsprSchF;
    }

    public String getNomKSchFPokup() {
        return nomKSchFPokup;
    }

    public void setNomKSchFPokup(String nomKSchFPokup) {
        this.nomKSchFPokup = nomKSchFPokup;
    }

    public String getDataKSchDPokup() {
        return dataKSchDPokup;
    }

    public void setDataKSchDPokup(String dataKSchDPokup) {
        this.dataKSchDPokup = dataKSchDPokup;
    }

    public String getNomIsprKSchF() {
        return nomIsprKSchF;
    }

    public void setNomIsprKSchF(String nomIsprKSchF) {
        this.nomIsprKSchF = nomIsprKSchF;
    }

    public String getDataIsprKSchf() {
        return dataIsprKSchf;
    }

    public void setDataIsprKSchf(String dataIsprKSchf) {
        this.dataIsprKSchf = dataIsprKSchf;
    }

    public String getKodVidOper() {
        return kodVidOper;
    }

    public void setKodVidOper(String kodVidOper) {
        this.kodVidOper = kodVidOper;
    }

    public String getInnPokup() {
        return innPokup;
    }

    public void setInnPokup(String innPokup) {
        this.innPokup = innPokup;
    }

    public String getKppPokup() {
        return kppPokup;
    }

    public void setKppPokup(String kppPokup) {
        this.kppPokup = kppPokup;
    }

    public String getOkvPokup() {
        return okvPokup;
    }

    public void setOkvPokup(String okvPokup) {
        this.okvPokup = okvPokup;
    }

    public String getRazStKSchFUmPokup() {
        return razStKSchFUmPokup;
    }

    public void setRazStKSchFUmPokup(String razStKSchFUmPokup) {
        this.razStKSchFUmPokup = razStKSchFUmPokup;
    }

    public String getRazStKSchAUvPokup() {
        return razStKSchAUvPokup;
    }

    public void setRazStKSchAUvPokup(String razStKSchAUvPokup) {
        this.razStKSchAUvPokup = razStKSchAUvPokup;
    }

    public String getRazNDSKSchFUmPokup() {
        return razNDSKSchFUmPokup;
    }

    public void setRazNDSKSchFUmPokup(String razNDSKSchFUmPokup) {
        this.razNDSKSchFUmPokup = razNDSKSchFUmPokup;
    }

    public String getRazNDSKSchFUvPokup() {
        return razNDSKSchFUvPokup;
    }

    public void setRazNDSKSchFUvPokup(String razNDSKSchFUvPokup) {
        this.razNDSKSchFUvPokup = razNDSKSchFUvPokup;
    }
}
