package entity;

import java.util.List;

/**
 * Created by konstantin on 12.02.15.
 */
public class SchetFaktura {

    String idFile;//TODO ok Идентификатор файла
    float versForm = 5.1f;//TODO ok
    String idPok = "ИдПокупателя";//TODO ok
    String idOtpr = "ИдОтправителя";//TODO ok
    String innOtpr = "7816151039";//TODO ok
    String kppOtpr = "783450001";//TODO ok
    String nameOtprav = "СоюзБалтКомплект";//TODO ok
    String idEDO = "000";//TODO ok
    String knd = "1115101";//TODO ok

//    <СвСчФакт НомерСчФ="a" КодОКВ="000" ДатаСчФ="31.12.2000">
    String sChFaktNumber;  //TODO ok
    String kodOKV = "643";          //TODO ok
    String dataSChFakt;     //TODO ok
    String kodVidOper;//КодВидОпер

//  <СвПрод>
//  <СвЮЛ НаимОрг="a"/>
//  <АдрРФ КодРегион="00"/>
    String svProdUlfirmName = "СоюзБалтКомплект";//TODO ok
    String svProdRegionCod = "00";//TODO ok

//  <ГрузОт>
//  <НаимОрг>a</НаимОрг>
//  <АдрРФ КодРегион="00"/>
    String gruzOtFirmName = svProdUlfirmName;//TODO ok
    String gruzOtkodRegion = svProdRegionCod;//TODO ok

//  <ГрузПолуч>
//  <НаимОрг>a</НаимОрг>
//  <АдрРФ КодРегион="00"/>

    String gruzPoluchFirName;//TODO ok
    String gruzPoluchCodRegion = "00";//TODO ok



//    <СвПокуп>
//    <ИдСв>
//    <СвЮЛ ИННЮЛ="1000000000" НаимОрг="a" КПП="100000000"/>
//    </ИдСв>
//    <Адрес>
//    <АдрРФ Корпус="a" Город="a" Улица="a" КодРегион="00" Кварт="a" НаселПункт="a" Дом="a" Район="a" Индекс="000000"/>
//    </Адрес>
//    </СвПокуп>
    String svPrdInnUl;
    String svPrdKppUl;
    String svPrdNumberPrd = "0";//TODO ok
    String svPrdDatePrd = "01.01.2014";//TODO ok

    public void setSvPokupUlfirmName(String svPokupUlfirmName) {
        this.svPokupUlfirmName = svPokupUlfirmName;
    }

    //    <СвПокуп>
//    <СвЮЛ НаимОрг="a"/>
//    <АдрРФ КодРегион="00"/>
    String svPokupUlfirmName;//TODO ok
    String svPokupUlRegionKod = gruzPoluchCodRegion;//TODO ok

//    <ТаблСчФакт>
//    <СведТов НомСтр="123456" СтТовУчНал="123456789123456.12" НаимТов="a" КолТов="123456789123456.123" ЦенаТов="123456789123456.12345678912" ОКЕИ_Тов="000" СтТовБезНДС="123456789123456.12" ИнфПолСтр="">
//    <Акциз СумАкциз="0000"/>
//    <НалСт НалСтВел="0" НалСтТип="дробь"/>
//    <СумНал СумНДС="0"/>
//    <КодПроисх>000</КодПроисх>
//    <НомерТД>aaaaaaaaaaaaaaaaaaaaaaaaaaaaa</НомерТД>
//    <ВсегоОпл СтТовУчНалВсего="123456789123456.12">
//    <СумНалВсего СумНДС="0"/>
    String nomStr; //Номер строки таблицы//TODO ok
    String stTovUchNal;//Стоимость товаров//TODO ok
    List<String> NomerTD;

    public String getStTovBezNDS() {
        return stTovBezNDS;
    }

    public void setStTovBezNDS(String stTovBezNDS) {
        this.stTovBezNDS = stTovBezNDS;
    }

    String stTovBezNDS;

    public String getSvedTovTsenaTov() {
        return svedTovTsenaTov;
    }

    public void setSvedTovTsenaTov(String svedTovTsenaTov) {
        this.svedTovTsenaTov = svedTovTsenaTov;
    }

    String svedTovTsenaTov;
    String svedTovNaimTovar = "Товары";
    String summaAkciza = "0000";//TODO ok
    String nalStavkaVelich = "0";//налоговая ставка - величина. 0%, 10% или 18 %//TODO ok

    public String getStTovBezNDSVsego() {
        return stTovBezNDSVsego;
    }

    public void setStTovBezNDSVsego(String stTovBezNDSVsego) {
        this.stTovBezNDSVsego = stTovBezNDSVsego;
    }

    String stTovBezNDSVsego = "0";//СтТовБезНДСВсего
    String nalStavkaTip = "процент";//налоговая ставка//TODO ok
    String sumNalSumNDS;//TODO ok
    String kodProishozhdeniaTovara = "000";//TODO ok
    String nomerTd = "aaaaaaaaaaaaaaaaaaaaa";//TODO ok
    String kOplateVsego;//TODO ok
    String sumNdsVsego;//TODO ok

//  Сведения о лице,подписывающем документ в электронном виде
//    <Подписант>
//    <ЮЛ ИННЮЛ="100000000000">
//    <ФИО Фамилия="" Имя="a"/>
    String podpisantInnUl = "7816151039";//TODO ok
    String podpisantFio = "Иванов Иван";//TODO ok

    String stoimostProdazhOsobOtNaloga = "0";

    public String getStoimostProdazhOsobOtNaloga() {
        return stoimostProdazhOsobOtNaloga;
    }

    public void setStoimostProdazhOsobOtNaloga(String stoimostProdazhOsobOtNaloga) {
        this.stoimostProdazhOsobOtNaloga = stoimostProdazhOsobOtNaloga;
    }

    public String getGruzPoluchFirName() {
        return gruzPoluchFirName;
    }

    public void setGruzPoluchFirName(String gruzPoluchFirName) {
        this.gruzPoluchFirName = gruzPoluchFirName;
    }

    public String getNomStr() {
        return nomStr;
    }

    public void setNomStr(String nomStr) {
        this.nomStr = nomStr;
    }

    public String getStTovUchNal() {
        return stTovUchNal;
    }

    public void setStTovUchNal(String stTovUchNal) {
        this.stTovUchNal = stTovUchNal;
    }

    public String getSumNalSumNDS() {
        return sumNalSumNDS;
    }

    public void setSumNalSumNDS(String sumNalSumNDS) {
        this.sumNalSumNDS = sumNalSumNDS;
    }

    public String getNalStavkaVelich() {
        return nalStavkaVelich;
    }

    public void setNalStavkaVelich(String nalStavkaVelich) {
        this.nalStavkaVelich = nalStavkaVelich;
    }

    public String getSvedTovNaimTovar() {
        return svedTovNaimTovar;
    }

    public void setSvedTovNaimTovar(String svedTovNaimTovar) {
        this.svedTovNaimTovar = svedTovNaimTovar;
    }

    public String getkOplateVsego() {
        return kOplateVsego;
    }

    public void setkOplateVsego(String kOplateVsego) {
        this.kOplateVsego = kOplateVsego;
    }

    public String getSumNdsVsego() {
        return sumNdsVsego;
    }

    public void setSumNdsVsego(String sumNdsVsego) {
        this.sumNdsVsego = sumNdsVsego;
    }

    public String getIdFile() {

        return idFile;
    }

    public void setIdFile(String idFile) {
        this.idFile = idFile;
    }

    public String getsChFaktNumber() {
        return sChFaktNumber;
    }

    public void setsChFaktNumber(String sChFaktNumber) {
        this.sChFaktNumber = sChFaktNumber;
    }

    public String getKodOKV() {
        return kodOKV;
    }

    public void setKodOKV(String kodOKV) {
        this.kodOKV = kodOKV;
    }

    public String getDataSChFakt() {
        return dataSChFakt;
    }

    public void setDataSChFakt(String dataSChFakt) {
        this.dataSChFakt = dataSChFakt;
    }

    public float getVersForm() {
        return versForm;
    }

    public String getIdPok() {
        return idPok;
    }

    public String getIdOtpr() {
        return idOtpr;
    }

    public String getInnOtpr() {
        return innOtpr;
    }

    public String getNameOtprav() {
        return nameOtprav;
    }

    public String getIdEDO() {
        return idEDO;
    }

    public String getKnd() {
        return knd;
    }

    public String getSvProdUlfirmName() {
        return svProdUlfirmName;
    }

    public String getSvProdRegionCod() {
        return svProdRegionCod;
    }

    public String getGruzOtFirmName() {
        return gruzOtFirmName;
    }

    public String getGruzOtkodRegion() {
        return gruzOtkodRegion;
    }

    public String getGruzPoluchCodRegion() {
        return gruzPoluchCodRegion;
    }

    public String getSvPrdNumberPrd() {
        return svPrdNumberPrd;
    }

    public String getSvPrdDatePrd() {
        return svPrdDatePrd;
    }

    public String getSvPokupUlfirmName() {
        return svPokupUlfirmName;
    }

    public String getSummaAkciza() {
        return summaAkciza;
    }

    public String getNalStavkaTip() {
        return nalStavkaTip;
    }

    public String getKodProishozhdeniaTovara() {
        return kodProishozhdeniaTovara;
    }

    public String getNomerTd() {
        return nomerTd;
    }

    public String getPodpisantInnUl() {
        return podpisantInnUl;
    }

    public String getPodpisantFio() {
        return podpisantFio;
    }

    public String getSvPokupUlRegionKod() {
        return svPokupUlRegionKod;
    }

    public void setSvPokupUlRegionKod(String svPokupUlRegionKod) {
        this.svPokupUlRegionKod = svPokupUlRegionKod;
    }

    public String getKppOtpr() {
        return kppOtpr;
    }

    public void setKppOtpr(String kppOtpr) {
        this.kppOtpr = kppOtpr;
    }

    public String getSvPrdInnUl() {
        return svPrdInnUl;
    }

    public void setSvPrdInnUl(String svPrdInnUl) {
        this.svPrdInnUl = svPrdInnUl;
    }

    public String getSvPrdKppUl() {
        return svPrdKppUl;
    }

    public void setSvPrdKppUl(String svPrdKppUl) {
        this.svPrdKppUl = svPrdKppUl;
    }

    public String getKodVidOper() {
        return kodVidOper;
    }

    public void setKodVidOper(String kodVidOper) {
        this.kodVidOper = kodVidOper;
    }

    public void setVersForm(float versForm) {
        this.versForm = versForm;
    }

    public void setIdPok(String idPok) {
        this.idPok = idPok;
    }

    public void setIdOtpr(String idOtpr) {
        this.idOtpr = idOtpr;
    }

    public void setInnOtpr(String innOtpr) {
        this.innOtpr = innOtpr;
    }

    public void setNameOtprav(String nameOtprav) {
        this.nameOtprav = nameOtprav;
    }

    public void setIdEDO(String idEDO) {
        this.idEDO = idEDO;
    }

    public void setKnd(String knd) {
        this.knd = knd;
    }

    public void setSvProdUlfirmName(String svProdUlfirmName) {
        this.svProdUlfirmName = svProdUlfirmName;
    }

    public void setSvProdRegionCod(String svProdRegionCod) {
        this.svProdRegionCod = svProdRegionCod;
    }

    public void setGruzOtFirmName(String gruzOtFirmName) {
        this.gruzOtFirmName = gruzOtFirmName;
    }

    public void setGruzOtkodRegion(String gruzOtkodRegion) {
        this.gruzOtkodRegion = gruzOtkodRegion;
    }

    public void setGruzPoluchCodRegion(String gruzPoluchCodRegion) {
        this.gruzPoluchCodRegion = gruzPoluchCodRegion;
    }

    public void setSvPrdNumberPrd(String svPrdNumberPrd) {
        this.svPrdNumberPrd = svPrdNumberPrd;
    }

    public void setSvPrdDatePrd(String svPrdDatePrd) {
        this.svPrdDatePrd = svPrdDatePrd;
    }

    public void setSummaAkciza(String summaAkciza) {
        this.summaAkciza = summaAkciza;
    }

    public void setNalStavkaTip(String nalStavkaTip) {
        this.nalStavkaTip = nalStavkaTip;
    }

    public void setKodProishozhdeniaTovara(String kodProishozhdeniaTovara) {
        this.kodProishozhdeniaTovara = kodProishozhdeniaTovara;
    }

    public void setNomerTd(String nomerTd) {
        this.nomerTd = nomerTd;
    }

    public void setPodpisantInnUl(String podpisantInnUl) {
        this.podpisantInnUl = podpisantInnUl;
    }

    public void setPodpisantFio(String podpisantFio) {
        this.podpisantFio = podpisantFio;
    }

    public List<String> getNomerTD() {
        return NomerTD;
    }

    public void setNomerTD(List<String> nomerTD) {
        NomerTD = nomerTD;
    }
}
