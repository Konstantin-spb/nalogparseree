package ejb;

import entity.EntityNoNds;
import entity.EntityRazdel10;
import entity.EntityRazdel11;
import entity.EntityRazdel8;
import entity.EntityRazdel9;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.io.FileUtils;
import xml.handlers.Razdel10;
import xml.handlers.Razdel11;
import xml.handlers.Razdel8;
import xml.handlers.Razdel9;
import xml.templates.NoNdsTemplate;
import xml.templates.Razdel10Template;
import xml.templates.Razdel11Template;
import xml.templates.Razdel8Template;
import xml.templates.Razdel9Template;

import java.io.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import com.eaio.uuid.UUID;

import javax.ejb.Stateless;

@Stateless
public class MakeAllReports {

	public static StringBuilder errors = new StringBuilder();
	private String zipFilename;

	SimpleDateFormat dateFormatDefis = new SimpleDateFormat("yyyy-MM-dd");
	SimpleDateFormat dateFormatSlitno = new SimpleDateFormat("yyyyMMdd");
	SimpleDateFormat dateFormatTochka = new SimpleDateFormat("dd.MM.yyyy");
	SimpleDateFormat dateFormatOnlyYear = new SimpleDateFormat("yyyy");

	public String go(List<FileItem> fileItems) {

		String kodIFNS = fileItems.get(0).getString();
		String inn = fileItems.get(1).getString();
		String kpp = fileItems.get(2).getString();
		String customDate = fileItems.get(3).getString();

//		List<String> stringList = new ArrayList<String>();
		Map<String, String> files = new HashMap<String, String>();

		String fileName8 = "";
		String fileName9 = "";
		String fileName10 = "";
		String fileName11 = "";
		String filenameNoNds = "";
		String xmlText;

		Pattern pattern = Pattern.compile("file[\\d]{1,2}");
		Matcher matcher;

		Iterator<FileItem> it = fileItems.iterator();
		while (it.hasNext()) {
			FileItem fileItem = it.next();
			boolean isFormField = fileItem.isFormField();
			if (isFormField) {
				// Не является файлом
				System.out.println(fileItem.getFieldName() + "=" + fileItem.getString());
			} else {
				// Является файлом
				matcher = pattern.matcher(fileItem.getFieldName());
				while (matcher.find()) {

					if (fileItem.getSize() > 0 && fileItem.getFieldName().equalsIgnoreCase("file8")) {
						System.out.println("файл " + fileItem.getFieldName() + " имеет размер " + fileItem.getSize());
						try {
							fileName8 = makeFileName(".8", kodIFNS, inn, kpp, customDate);
							xmlText = razdel8(fileItem.getInputStream(), fileName8);
							files.put(fileName8, xmlText);

//							fileName8 = "";
							xmlText = "";
//							System.out.println();
						} catch (IOException e) {
							e.printStackTrace();
						}
					} else if (fileItem.getSize() > 0 && fileItem.getFieldName().equalsIgnoreCase("file9")) {
						System.out.println("файл " + fileItem.getFieldName() + " имеет размер " + fileItem.getSize());
						try {
							fileName9 = makeFileName(".9", kodIFNS, inn, kpp, customDate);
							xmlText = razdel9(fileItem.getInputStream(), fileName9);
							files.put(fileName9, xmlText);

//							fileName9 = "";
							xmlText = "";
						} catch (IOException e) {
							e.printStackTrace();
						}

					} else if (fileItem.getSize() > 0 && fileItem.getFieldName().equalsIgnoreCase("file10")) {
						System.out.println("файл " + fileItem.getFieldName() + " имеет размер " + fileItem.getSize());
						try {
							fileName10 = makeFileName(".10", kodIFNS, inn, kpp, customDate);
							xmlText = razdel10(fileItem.getInputStream(), fileName10);
							files.put(fileName10, xmlText);

//							fileName10 = "";
							xmlText = "";
						} catch (IOException e) {
							e.printStackTrace();
						}

					} else if (fileItem.getSize() > 0 && fileItem.getFieldName().equalsIgnoreCase("file11")) {
						System.out.println("файл " + fileItem.getFieldName() + " имеет размер " + fileItem.getSize());
						try {
							fileName11 = makeFileName(".11", kodIFNS, inn, kpp, customDate);
							xmlText = razdel11(fileItem.getInputStream(), fileName11);
							files.put(fileName11, xmlText);

//							fileName11 = "";
							xmlText = "";
						} catch (IOException e) {
							e.printStackTrace();
						}

					} else {
						System.out.println("файл " + fileItem.getFieldName() + " пустой " + fileItem.getSize());
					}
				}
			}
		}

		filenameNoNds = makeFileName("", kodIFNS, inn, kpp, customDate);
		try {
			files.put(filenameNoNds, razdelNoNds(fileItems, filenameNoNds, fileName8, fileName9,fileName10,fileName11));
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}

		createZipArchive(files);

		System.out.println("Программа закончила свою работу");
		System.out.println("название zip файла" + zipFilename);

		return zipFilename;
	}

	public String razdelNoNds(List<FileItem> fileItems,String filenameNoNds, String filename8, String filename9, String filename10, String filename11) throws UnsupportedEncodingException {
		String xmlText = null;

		EntityNoNds entityNoNds = new EntityNoNds();

		String kodIFNS = fileItems.get(0).getString();
		String inn = fileItems.get(1).getString();
		String kpp = fileItems.get(2).getString();
//		String customDate = fileItems.get(3).getString();

		entityNoNds.setKodNo(kodIFNS);
		entityNoNds.setInnUl(inn);
		entityNoNds.setKpp(kpp);
		try {
			entityNoNds.setCustomDate(dateFormatTochka.format(dateFormatDefis.parse(fileItems.get(3).getString())));
		} catch (ParseException e) {
			e.printStackTrace();
		}

		entityNoNds.setKnd(new String(fileItems.get(4).get(), "UTF-8"));
		entityNoNds.setPoMestu(new String(fileItems.get(5).get(), "UTF-8"));
		entityNoNds.setNomKorr(new String(fileItems.get(6).get(), "UTF-8"));
		entityNoNds.setOkved(new String(fileItems.get(7).get(), "UTF-8"));
		entityNoNds.setNaimOrg((new String(fileItems.get(8).get(), "UTF-8").replaceAll("\\\"", "&quot;")));
		entityNoNds.setFamiliya(new String(fileItems.get(9).get(), "UTF-8"));
		entityNoNds.setImya(new String(fileItems.get(10).get(), "UTF-8"));
		entityNoNds.setOtchestvo(new String(fileItems.get(11).get(), "UTF-8"));
		entityNoNds.setPrPodp(new String(fileItems.get(12).get(), "UTF-8"));
		entityNoNds.setOktmo(new String(fileItems.get(13).get(), "UTF-8"));
		entityNoNds.setKbk(new String(fileItems.get(14).get(), "UTF-8"));
		entityNoNds.setNaimKnPok(filename8);
		entityNoNds.setNaimKnProd(filename9);
		entityNoNds.setNaimZhuchVistSchF(filename10);
		entityNoNds.setNaimZhuchPoluchSchF(filename11);
		entityNoNds.setPeriod("2" + fileItems.get(15).getString());
		try {
			entityNoNds.setOtchetGod(dateFormatOnlyYear.format(dateFormatDefis.parse(fileItems.get(3).getString())));
		} catch (ParseException e) {
			e.printStackTrace();
		}
//		String s = fileItems.get(15).getString();
		entityNoNds.setIdfail(filenameNoNds);

		xmlText = new NoNdsTemplate().createXml(entityNoNds);

		return xmlText;
	}

	public String razdel8(InputStream in, String fileName) {
		List<EntityRazdel8> entityRazdel8s = new Razdel8().parseRazdel8(in);
		String xmlText = new Razdel8Template().createXml(entityRazdel8s, fileName);

		return xmlText;
	}

	public String razdel9(InputStream in, String fileName) {
		List<EntityRazdel9> entityRazdel9s = new Razdel9().parseRazdel9(in);
		String xmlText = new Razdel9Template().createXml(entityRazdel9s, fileName);

		return xmlText;
	}

	public void razdel9Prilozhenie1(String fileName) {}

	public String razdel10(InputStream in, String fileName) {
		List<EntityRazdel10> entityRazdel10s = new Razdel10().parseRazdel10(in);
		String xmlText = new Razdel10Template().createXml(entityRazdel10s, fileName);

		return xmlText;
	}

	public String razdel11(InputStream in, String fileName) {

		List<EntityRazdel11> entityRazdel11s = new Razdel11().parseRazdel11(in);
		String xmlText = new Razdel11Template().createXml(entityRazdel11s, fileName);

		return xmlText;
	}

//	public String makeFileName(String s) {
//		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");
//		Date currentDate = new Date();
//		UUID u = new UUID();
//		String idFile = s + dateFormat.format(currentDate) + "_" + u;
//
//		return idFile;
//	}

	public void createZipArchive(Map<String, String> files) {
		// Create a buffer for reading the files
		byte[] buf = new byte[1024];
        FileInputStream in;

//		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");
		Date currentDate = new Date();
		UUID u = new UUID();

		try {
			// Create the ZIP file
			zipFilename = "target_" + dateFormatSlitno.format(currentDate) + "_" + u + ".zip";
			ZipOutputStream out = new ZipOutputStream(new FileOutputStream("/tmp/" + zipFilename));

			for (Map.Entry<String, String> stringStringEntry : files.entrySet()) {

                String s = "/tmp/" + stringStringEntry.getKey() + ".xml";

				File file = new File(s);
				try {
					if (!file.exists()) {
						file.createNewFile();
					}
					FileUtils.write(file, stringStringEntry.getValue(), "windows-1251");
				} catch (IOException e) {
					e.printStackTrace();
				} finally {
					file = null;
				}

                in = new FileInputStream(s);

                // Add ZIP entry to output stream.
				out.putNextEntry(new ZipEntry(stringStringEntry.getKey() + ".xml"));

				// Transfer bytes from the file to the ZIP file
				int len;
				while ((len = in.read(buf)) > 0) {
					out.write(buf, 0, len);
				}

				// Complete the entry
				out.closeEntry();
				in.close();
			}
			out.close();
		} catch (IOException e) {

		}

		System.out.println();
	}

	public String makeFileName(String nomerRazdela, String kodIFNS, String inn, String kpp, String customDate) {

		Date currentDate = null;
		try {
			currentDate = dateFormatDefis.parse(customDate);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		UUID u = new UUID();
		// String idFile = s + dateFormatDefis.format(currentDate) + "_" + u;
		String idFile = "NO_NDS" + nomerRazdela + "_" + kodIFNS + "_" + kodIFNS + "_" + inn + "" + kpp + "_"
				+ dateFormatSlitno.format(currentDate) + "_" + u;

		return idFile;
	}

}