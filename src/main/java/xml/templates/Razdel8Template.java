package xml.templates;

import entity.EntityRazdel8;

import java.util.List;

/**
 * Created by konstantin on 13.03.15.
 */
public class Razdel8Template {
    public String createXml(List<EntityRazdel8> entityRazdel8s, String fileName) {

        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("<?xml version=\"1.0\" encoding=\"windows-1251\"?>\n");
        stringBuilder.append("<Файл xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" ИдФайл=\"").append(fileName).append("\" ВерсПрог=\"Налогоплательщик ЮЛ 4.40.4\" ВерсФорм=\"5.04\">\n");
        stringBuilder.append("<Документ Индекс=\"0000080\" НомКорр=\"0\">\n");
        stringBuilder.append("<КнигаПокуп СумНДСВсКПк=\"0\">\n");

        int i = 1;
        for (EntityRazdel8 entityRazdel8: entityRazdel8s) {
            stringBuilder.append("<КнПокСтр НомерПор=\"").append(i);
            if (entityRazdel8.getNomSchFProd() != null) {
                stringBuilder.append("\" НомСчФПрод=\"").append(entityRazdel8.getNomSchFProd());
            }
            if (entityRazdel8.getDataSchFProd()!= null) {
                stringBuilder.append("\" ДатаСчФПрод=\"").append(entityRazdel8.getDataSchFProd());
            }

            if (entityRazdel8.getNomIspSchF()!= null) {
                stringBuilder.append("\" НомИспрСчФ=\"").append(entityRazdel8.getNomIspSchF());
            }
            if (entityRazdel8.getDataIsprSchF()!= null) {
                stringBuilder.append("\" ДатаИспрСчФ=\"").append(entityRazdel8.getDataIsprSchF());
            }
            if (entityRazdel8.getNomKSchFProd()!= null) {
                stringBuilder.append("\" НомКСчФПрод=\"").append(entityRazdel8.getNomKSchFProd());
            }
            if (entityRazdel8.getDataKSchFProd()!= null) {
                stringBuilder.append("\" ДатаКСчФПрод=\"").append(entityRazdel8.getDataKSchFProd());
            }
            if (entityRazdel8.getNomIsprKSchF()!= null) {
                stringBuilder.append("\" НомИспрКСчФ=\"").append(entityRazdel8.getNomIsprKSchF());
            }
            if (entityRazdel8.getDataIsprKSchF()!= null) {
                stringBuilder.append("\" ДатаИспрКСчФ=\"").append(entityRazdel8.getDataIsprKSchF());
            }

            if (entityRazdel8.getNomTD()!= null) {
                stringBuilder.append("\" НомТД=\"").append(entityRazdel8.getNomTD());
            }

            if (entityRazdel8.getOkv() != null) {
                stringBuilder.append("\" ОКВ=\"").append(entityRazdel8.getOkv());
            }
            if (entityRazdel8.getStoimPokupV() != null) {
                stringBuilder.append("\" СтоимПокупВ=\"").append(entityRazdel8.getStoimPokupV());
            }
            if (entityRazdel8.getSumNdsVych() != null) {
                stringBuilder.append("\" СумНДСВыч=\"").append(entityRazdel8.getSumNdsVych());
            }
            stringBuilder.append("\">\n");
            stringBuilder.append("<КодВидОпер>").append(entityRazdel8.getKodVidOper()).append("</КодВидОпер>\n");
//            if (entityRazdel8.getNomDokPdtvUpl() != null && entityRazdel8.getDataDokPdtvUpl() != null) {
            if(entityRazdel8.getNomAndDatePdtvUpl() != null) {
                String[] srt = entityRazdel8.getNomAndDatePdtvUpl().split("\\|\\|");
                for (String s : srt) {
                    String[] temp = s.trim().split("\\s+");
                    stringBuilder.append("<ДокПдтвУпл НомДокПдтвУпл=\"").append(temp[0]).append("\" ДатаДокПдтвУпл=\"").append(temp[1]).append("\"/>\n");
                }
//                stringBuilder.append("<ДокПдтвУпл НомДокПдтвУпл=\"").append(entityRazdel8.getNomDokPdtvUpl()).append("\" ДатаДокПдтвУпл=\"").append(entityRazdel8.getDataDokPdtvUpl()).append("\"/>\n");
            }
            if (entityRazdel8.getDataUchTov() != null) {
                stringBuilder.append("<ДатаУчТов>").append(entityRazdel8.getDataUchTov()).append("</ДатаУчТов>\n");
            }

//            <КнПокСтр НомерПор="1" НомСчФПрод="123" ДатаСчФПрод="02.03.2015" НомИспрСчФ="23" ДатаИспрСчФ="03.03.2015" НомКСчФПрод="123446"
//            ДатаКСчФПрод="04.03.2015" НомИспрКСчФ="345" ДатаИспрКСчФ="05.03.2015" ОКВ="643" СтоимПокупВ="10000.00" СумНДСВыч="1800">
//            <КодВидОпер>01</КодВидОпер>
//            <ДокПдтвУпл НомДокПдтвУпл="5643" ДатаДокПдтвУпл="04.02.2015" />

            if (entityRazdel8.getInnProd() != null && entityRazdel8.getInnProd().length() == 10) {
                stringBuilder.append("<СвПрод>\n");
                stringBuilder.append("<СведЮЛ ИННЮЛ=\"").append(entityRazdel8.getInnProd()).append("\" ");
                if (entityRazdel8.getKppProd() != null) {
                    stringBuilder.append(" КПП=\"").append(entityRazdel8.getKppProd()).append("\" ");
                }
                stringBuilder.append("/>\n");
                stringBuilder.append("</СвПрод>\n");
            }

            if (entityRazdel8.getInnProd() != null && entityRazdel8.getInnProd().length() == 12) {
                stringBuilder.append("<СвПрод>\n");
                stringBuilder.append("<СведИП ИННФЛ=\"").append(entityRazdel8.getInnProd()).append("\" ");
                stringBuilder.append("/>\n");

                stringBuilder.append("</СвПрод>\n");
            }

            stringBuilder.append("</КнПокСтр>\n");
            i++;
        }

        stringBuilder.append("\n</КнигаПокуп>\n" +
                "</Документ>\n" +
                "</Файл>");
        return stringBuilder.toString();
    }
}