package xml.templates;

import entity.EntityRazdel9;

import java.util.List;

/**
 * Created by konstantin on 16.03.15.
 */
public class Razdel9Template {

    public String createXml(List<EntityRazdel9> entityRazdel9s, String fileName) {

        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("<?xml version=\"1.0\" encoding=\"windows-1251\"?>\n" +
                "<Файл xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" ИдФайл=\"").append(fileName).append("\" ВерсПрог=\"Налогоплательщик ЮЛ 4.40.4\" ВерсФорм=\"5.04\">\n" +
                "<Документ Индекс=\"0000090\" НомКорр=\"0\">\n" +
                "<КнигаПрод>\n");

        int i = 1;
        for (EntityRazdel9 entityRazdel9 : entityRazdel9s) {
            stringBuilder.append("<КнПродСтр НомерПор=\"");
            stringBuilder.append(i);

            stringBuilder.append("\" НомСчФПрод=\"");
            stringBuilder.append(entityRazdel9.getNomSchFProd());

            stringBuilder.append("\" ДатаСчФПрод=\"");
            stringBuilder.append(entityRazdel9.getDataSchFProd());
//НомКСчФПрод="432" ДатаКСчФПрод="03.03.2015"
            if (entityRazdel9.getNomKSchFProd() != null) {
                stringBuilder.append("\" НомКСчФПрод=\"");
                stringBuilder.append(entityRazdel9.getNomKSchFProd());
            }

            if (entityRazdel9.getDataKSchFProd() != null) {
                stringBuilder.append("\" ДатаКСчФПрод=\"");
                stringBuilder.append(entityRazdel9.getDataKSchFProd());
            }

            if (entityRazdel9.getNomIsprSchF() != null) {
                stringBuilder.append("\" НомИспрСчФ=\"");
                stringBuilder.append(entityRazdel9.getNomIsprSchF());
            }

            if (entityRazdel9.getDataIcprSchF() != null) {
                stringBuilder.append("\" ДатаИспрСчФ=\"");
                stringBuilder.append(entityRazdel9.getDataIcprSchF());
            }

            if (entityRazdel9.getNomIsprKSchF() != null) {
                stringBuilder.append("\" НомИспрКСчФ=\"");
                stringBuilder.append(entityRazdel9.getNomIsprKSchF());
            }

            if (entityRazdel9.getDataIsprKschF() != null) {
                stringBuilder.append("\" ДатаИспрКСчФ=\"");
                stringBuilder.append(entityRazdel9.getDataIsprKschF());
            }

            if (entityRazdel9.getOkv() != null) {
                stringBuilder.append("\" ОКВ=\"");
                stringBuilder.append(entityRazdel9.getOkv());
            }

            if (entityRazdel9.getStoimProdSFV() != null) {
                stringBuilder.append("\" СтоимПродСФВ=\"");
                stringBuilder.append(entityRazdel9.getStoimProdSFV());
            }

            if (entityRazdel9.getStoimProdSF() != null) {
                stringBuilder.append("\" СтоимПродСФ=\"");
                stringBuilder.append(entityRazdel9.getStoimProdSF());
            }

            if (entityRazdel9.getStoimProdSF18_10() != null) {
                stringBuilder.append("\" СтоимПродСФ18=\"");
                stringBuilder.append(entityRazdel9.getStoimProdSF18_10());
            }

            if (entityRazdel9.getSumNDSSF() != null) {
                stringBuilder.append("\" СумНДССФ18=\"");
                stringBuilder.append(entityRazdel9.getSumNDSSF());
            }

            if (entityRazdel9.getStoimProdSF0() != null) {
                stringBuilder.append("\" СтоимПродСФ0=\"");
                stringBuilder.append(entityRazdel9.getStoimProdSF0());
            }

            if (entityRazdel9.getStoimProdOsv() != null) {
                stringBuilder.append("\" СтоимПродОсв=\"");
                stringBuilder.append(entityRazdel9.getStoimProdOsv());
            }

            stringBuilder.append("\">\n");

            stringBuilder.append("<КодВидОпер>").append(entityRazdel9.getKodVidOper()).append("</КодВидОпер>\n");

            if (entityRazdel9.getNomDokPdtvOpl() != null && entityRazdel9.getDataDokPdtvOpl() != null) {
                stringBuilder.append("<ДокПдтвОпл НомДокПдтвОпл=\"")
                        .append(entityRazdel9.getNomDokPdtvOpl());

                stringBuilder.append("\" ДатаДокПдтвОпл=\"")
                        .append(entityRazdel9.getDataDokPdtvOpl())
                        .append("\"/>\n");
            }

//            if (entityRazdel9.getInnPokup() != null && !entityRazdel9.getInnPokup().isEmpty()) {

//                stringBuilder.append("<СведЮЛ ИННЮЛ=\"").append(entityRazdel9.getInnPokup()).append("\" КПП=\"").append(entityRazdel9.getKppPokup()).append("\"/>\n");
            if (entityRazdel9.getInnPokup() != null && entityRazdel9.getInnPokup().length() == 10) {
                stringBuilder.append("<СвПокуп>\n");
                stringBuilder.append("<СведЮЛ ИННЮЛ=\"").append(entityRazdel9.getInnPokup()).append("\" ");
                if (entityRazdel9.getKppPokup() != null) {
                    stringBuilder.append(" КПП=\"").append(entityRazdel9.getKppPokup()).append("\" ");
                }
                stringBuilder.append("/>\n");
                stringBuilder.append("</СвПокуп>\n");
            } else if (entityRazdel9.getInnPokup() != null && entityRazdel9.getInnPokup().length() == 12) {
                stringBuilder.append("<СвПокуп>\n");
                stringBuilder.append("<СведИП ИННФЛ=\"").append(entityRazdel9.getInnPokup()).append("\" ");
                stringBuilder.append("/>\n");
                stringBuilder.append("</СвПокуп>\n");
            }

            if (entityRazdel9.getInnPosrednik() != null && entityRazdel9.getInnPosrednik().length() == 10) {
                stringBuilder.append("<СвПокуп>\n");
                stringBuilder.append("<СведЮЛ ИННЮЛ=\"").append(entityRazdel9.getInnPosrednik()).append("\" ");
                if (entityRazdel9.getKppPosrednik() != null) {
                    stringBuilder.append(" КПП=\"").append(entityRazdel9.getKppPosrednik()).append("\" ");
                }
                stringBuilder.append("/>\n");
                stringBuilder.append("</СвПокуп>\n");
            } else if (entityRazdel9.getInnPosrednik() != null && entityRazdel9.getInnPosrednik().length() == 12) {
                stringBuilder.append("<СвПокуп>\n");
                stringBuilder.append("<СведИП ИННФЛ=\"").append(entityRazdel9.getInnPosrednik()).append("\" ");
                stringBuilder.append("/>\n");
                stringBuilder.append("</СвПокуп>\n");
            }

//            if (entityRazdel9.getInnPosrednik() != null && !entityRazdel9.getInnPosrednik().isEmpty()) {
//                stringBuilder.append("<СвПос>\n");
//                stringBuilder.append("<СведЮЛ ИННЮЛ=\"").append(entityRazdel9.getInnPosrednik()).append("\" КПП=\"").append(entityRazdel9.getKppPosrednik()).append("\"/>\n");
//                stringBuilder.append("</СвПос>\n");
//            }
            stringBuilder.append("</КнПродСтр>\n");
            i++;
        }

        stringBuilder.append("</КнигаПрод>\n");
        stringBuilder.append("</Документ>\n");
        stringBuilder.append("</Файл>\n");

        return stringBuilder.toString();
    }
}
