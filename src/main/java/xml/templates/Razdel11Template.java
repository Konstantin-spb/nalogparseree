package xml.templates;

import entity.EntityRazdel11;

import java.util.List;

/**
 * Created by konstantin on 24.03.15.
 */
public class Razdel11Template {

    public String createXml(List<EntityRazdel11> entityRazdel11s, String fileName) {

        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("<?xml version=\"1.0\" encoding=\"windows-1251\" ?>\n" +
                "<Файл xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" ИдФайл=\"").append(fileName).append("\" ВерсПрог=\"Налогоплательщик ЮЛ 4.40.4\" ВерсФорм=\"5.04\">\n" +
                "<Документ Индекс=\"0000110\" НомКорр=\"0\">\n" +
                "<ЖУчПолучСчФ>\n");

//<ЖУчПолучСчФСтр НомерПор="148"
// НомИспрСчФ="222"
// ДатаИспрСчФ="04.06.2015"
// НомКСчФПрод="33333333333"
// ДатаКСчФПрод="10.06.2015"
// НомИспрКСчФ="555"
// ДатаИспрКСчФ="18.06.2015"
// РазСтКСчФУм="12"
// РазСтКСчФУв="3"
// РазНДСКСчФУм="4"
// РазНДСКСчФУв="5">

        for (EntityRazdel11 entityRazdel11 : entityRazdel11s) {

            if (entityRazdel11.getNomSchFProd() != null && !entityRazdel11.getNomSchFProd().isEmpty()) {
                stringBuilder.append("<ЖУчПолучСчФСтр НомерПор=\"").append(entityRazdel11s.indexOf(entityRazdel11) + 1);
                stringBuilder.append("\" ДатаПолуч=\"").append(entityRazdel11.getDataPoluch());
                stringBuilder.append("\" НомСчФПрод=\"").append(entityRazdel11.getNomSchFProd());
                stringBuilder.append("\" ДатаСчФПрод=\"").append(entityRazdel11.getDataSchFProd());
                stringBuilder.append("\" КодВидСд=\"").append(entityRazdel11.getKodVidSd());
                stringBuilder.append("\" ОКВ=\"").append(entityRazdel11.getOkv());
                stringBuilder.append("\" СтоимТовСчФВс=\"").append(entityRazdel11.getStoimTovSchFVs());

                if (entityRazdel11.getSumNDSSchF() != null) {
                    stringBuilder.append("\" СумНДССчФ=\"").append(entityRazdel11.getSumNDSSchF());
                }

                if (entityRazdel11.getNomIsprSchF() != null) {
                    stringBuilder.append("\" НомИспрСчФ=\"").append(entityRazdel11.getNomIsprSchF());
                }
                if (entityRazdel11.getDataIsprSchF() != null) {
                    stringBuilder.append("\" ДатаИспрСчФ=\"").append(entityRazdel11.getDataIsprSchF());
                }
                if (entityRazdel11.getNomKSchFProd() != null) {
                    stringBuilder.append("\" НомКСчФПрод=\"").append(entityRazdel11.getNomKSchFProd());
                }
                if (entityRazdel11.getDataKSchFProd() != null) {
                    stringBuilder.append("\" ДатаКСчФПрод=\"").append(entityRazdel11.getDataKSchFProd());
                }
                if (entityRazdel11.getNomIsprKSchF() != null) {
                    stringBuilder.append("\" НомИспрКСчФ=\"").append(entityRazdel11.getNomIsprKSchF());
                }
                if (entityRazdel11.getDataIsprKSchF() != null) {
                    stringBuilder.append("\" ДатаИспрКСчФ=\"").append(entityRazdel11.getDataIsprKSchF());
                }
                if (entityRazdel11.getRazStKSchFUm() != null) {
                    stringBuilder.append("\" РазСтКСчФУм=\"").append(entityRazdel11.getRazStKSchFUm());
                }
                if (entityRazdel11.getRazStKSchFUv() != null) {
                    stringBuilder.append("\" РазСтКСчФУв=\"").append(entityRazdel11.getRazStKSchFUv());
                }
                if (entityRazdel11.getRazNDSKSchFUm() != null) {
                    stringBuilder.append("\" РазНДСКСчФУм=\"").append(entityRazdel11.getRazNDSKSchFUm());
                }
                if (entityRazdel11.getRazNDSKSchFUv() != null) {
                    stringBuilder.append("\" РазНДСКСчФУв=\"").append(entityRazdel11.getRazNDSKSchFUv());
                }


                stringBuilder.append("\">\n");
                stringBuilder.append("<КодВидОпер>").append(entityRazdel11.getKodVidOper());
                stringBuilder.append("</КодВидОпер> \n");

                stringBuilder.append("<СвПрод>\n");

                if (entityRazdel11.getInnProd() != null && entityRazdel11.getInnProd().length() == 10) {
                    stringBuilder.append("<СведЮЛ ИННЮЛ=\"").append(entityRazdel11.getInnProd()).append("\" ");
                    if (entityRazdel11.getKppProd() != null) {
                        stringBuilder.append(" КПП=\"").append(entityRazdel11.getKppProd()).append("\" ");
                    }
                    stringBuilder.append("/>\n");
                }

                if (entityRazdel11.getInnProd() != null && entityRazdel11.getInnProd().length() == 12) {
                    stringBuilder.append("<СведИП ИННФЛ=\"").append(entityRazdel11.getInnProd()).append("\" ");
                    stringBuilder.append("/>\n");
                }

                stringBuilder.append("</СвПрод>\n");
                stringBuilder.append("</ЖУчПолучСчФСтр>\n");
            }
        }

        stringBuilder.append("</ЖУчПолучСчФ>\n");
        stringBuilder.append("</Документ>\n");
        stringBuilder.append("</Файл>");

        return stringBuilder.toString();
    }

}
