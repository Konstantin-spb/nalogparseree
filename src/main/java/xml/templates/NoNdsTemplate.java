package xml.templates;

import entity.EntityNoNds;

/**
 * Created by konstantin on 12.08.15.
 */
public class NoNdsTemplate {

	public String createXml(EntityNoNds entityNoNds) {
		StringBuilder stringBuilder = new StringBuilder();

		stringBuilder.append("<?xml version=\"1.0\" encoding=\"windows-1251\"?>\n");
		stringBuilder.append("<Файл xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" ИдФайл=\"").append(entityNoNds.getIdfail())
				.append("\" ВерсПрог=\"Налогоплательщик ЮЛ 4.41.1\" ВерсФорм=\"5.04\" ПризнНал8-12=\"1\" ПризнНал8=\"1\" ПризнНал81=\"0\" ПризнНал9=\"1\" ПризнНал91=\"0\" ПризнНал10=\"1\" ПризнНал11=\"1\" ПризнНал12=\"0\">\n");
		stringBuilder.append(
				"<Документ КНД=\"").append(entityNoNds.getKnd()).append("\" ДатаДок=\"").append(entityNoNds.getCustomDate()).append("\" Период=\"").append(entityNoNds.getPeriod()).append("\" ОтчетГод=\"").append(entityNoNds.getOtchetGod()).append("\" КодНО=\"").append(entityNoNds.getKodNo()).append("\" НомКорр=\"").append(entityNoNds.getNomKorr()).append("\" ПоМесту=\"").append(entityNoNds.getPoMestu()).append("\">\n");
		stringBuilder.append("<СвНП ОКВЭД=\"").append(entityNoNds.getOkved()).append("\">\n");
		stringBuilder.append(
				"<НПЮЛ НаимОрг=\"").append(entityNoNds.getNaimOrg()).append("\" ИННЮЛ=\"").append(entityNoNds.getInnUl()).append("\" КПП=\"").append(entityNoNds.getKpp()).append("\"/>\n");
		stringBuilder.append("</СвНП><Подписант ПрПодп=\"").append(entityNoNds.getPrPodp()).append("\">\n");
		stringBuilder.append("<ФИО Фамилия=\"").append(entityNoNds.getFamiliya()).append("\" Имя=\"").append(entityNoNds.getImya()).append("\" Отчество=\"").append(entityNoNds.getOtchestvo()).append("\"/>\n");
		stringBuilder.append("</Подписант>\n");
		stringBuilder.append("<НДС>\n");
		stringBuilder.append("<СумУплНП ОКТМО=\"").append(entityNoNds.getOktmo()).append("\" КБК=\"").append(entityNoNds.getKbk()).append("\" СумПУ_173.1=\"26022587\"/>\n");
		stringBuilder.append("<СумУпл164 НалПУ164=\"26022587\">\n");
		stringBuilder.append("<СумНалОб НалВосстОбщ=\"26022587\">\n");
		stringBuilder.append("<РеалТов18 НалБаза=\"144569925\" СумНал=\"26022587\"/>\n");
		stringBuilder.append("</СумНалОб>\n");
		stringBuilder.append("</СумУпл164>\n");
		stringBuilder.append(
				"<КнигаПокуп НаимКнПок=\"").append(entityNoNds.getNaimKnPok()).append(".xml\"/>\n");
		stringBuilder.append(
				"<КнигаПрод НаимКнПрод=\"").append(entityNoNds.getNaimKnProd()).append(".xml\"/>\n");
		stringBuilder.append(
				"<ЖУчВыстСчФ НаимЖУчВыстСчФ=\"").append(entityNoNds.getNaimZhuchVistSchF()).append(".xml\"/>\n");
		stringBuilder.append(
				"<ЖУчПолучСчФ НаимЖУчПолучСчФ=\"").append(entityNoNds.getNaimZhuchPoluchSchF()).append(".xml\"/>\n");
		stringBuilder.append("</НДС>\n");
		stringBuilder.append("</Документ>\n");
		stringBuilder.append("</Файл>\n");

		return stringBuilder.toString();
	}
}
// <?xml version="1.0" encoding="windows-1251"?>
// <Файл xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
// ИдФайл="NO_NDS_7834_7834_7816151039783450001_20150702_11370DB1-37F3-864B-B2CC-9950321AB005"
// ВерсПрог="Налогоплательщик ЮЛ 4.41.1" ВерсФорм="5.04" ПризнНал8-12="1"
// ПризнНал8="1" ПризнНал81="0" ПризнНал9="1" ПризнНал91="0" ПризнНал10="1"
// ПризнНал11="1" ПризнНал12="0">
// <Документ КНД="1151001" ДатаДок="27.02.2015" Период="21" ОтчетГод="2015"
// КодНО="7834" НомКорр="0" ПоМесту="213">
// <СвНП ОКВЭД="20.30.1" Тлф="(812)244-46-00">
// <НПЮЛ НаимОрг="Общество с ограниченной ответственностью
// &quot;СоюзБалтКомплект&quot;" ИННЮЛ="7816151039" КПП="783450001"/>
// </СвНП><Подписант ПрПодп="1">
// <ФИО Фамилия="Кошицкий" Имя="Ярослав" Отчество="Иванович"/>
// </Подписант>
// <НДС>
// <СумУплНП ОКТМО="50015268" КБК="18210301000011000110"
// СумПУ_173.1="26022587"/>
// <СумУпл164 НалПУ164="26022587">
// <СумНалОб НалВосстОбщ="26022587">
// <РеалТов18 НалБаза="144569925" СумНал="26022587"/>
// </СумНалОб>
// </СумУпл164>
// <КнигаПокуп
// НаимКнПок="NO_NDS.8_7834_7834_7816151039783450001_20150702_E9BC2373-B1B6-C345-A166-CA1BCE8A38AE.xml"/>
// <КнигаПрод
// НаимКнПрод="NO_NDS.9_7834_7834_7816151039783450001_20150702_39E15714-451C-8743-99A9-EA5EDF79A318.xml"/>
// <ЖУчВыстСчФ
// НаимЖУчВыстСчФ="NO_NDS.10_7834_7834_7816151039783450001_20150702_58900656-F8B1-3D4A-8AD4-1FF3D000E495.xml"/>
// <ЖУчПолучСчФ
// НаимЖУчПолучСчФ="NO_NDS.11_7834_7834_7816151039783450001_20150702_A177FF0D-2408-1B4C-B899-883C6BC23171.xml"/>
// </НДС>
// </Документ>
// </Файл>