package xml.templates;

import entity.EntityRazdel10;
import entity.subEntities.SubEntities10Prod;

import java.util.List;

/**
 * Created by konstantin on 18.03.15.
 */
public class Razdel10Template {

//    <?xml version="1.0" encoding="windows-1251" ?>
//    <Файл xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" ИдФайл="NO_NDS.10_7834_7834_7816151039783450001_20150318_C702459C-658A-5D49-9B4C-6AEE32966D9B" ВерсПрог="Налогоплательщик ЮЛ 4.40.4" ВерсФорм="5.04">
//    <Документ Индекс="0000100" НомКорр="0">
//    <ЖУчВыстСчФ>
//        <ЖУчВыстСчФСтр НомерПор="1" ДатаВыст="10.03.2015" НомСчФПрод="123" ДатаСчФПрод="02.03.2015" НомИспрСчФ="23" ДатаИспрСчФ="04.03.2015" НомКСчФПрод="234" ДатаКСчФПрод="11.03.2015" НомИспрКСчФ="34" ДатаИспрКСчФ="17.03.2015">
//              <КодВидОпер>01</КодВидОпер>
//              <СвПокуп>
//                      <СведЮЛ ИННЮЛ="2801092430" КПП="280101001" />
//                  </СвПокуп>
//                  <СвПосрДеят НомСчФОтПрод="987" ДатаСчФОтПрод="02.03.2015" ОКВ="643" РазСтКСчФУм="100.00" РазСтКСчФУв="120.00" РазНДСКСчФУм="130" РазНДСКСчФУв="140">
//                  <СвПрод>
//                      <СведЮЛ ИННЮЛ="5021013190" КПП="502101001" />
//                  </СвПрод>
//              </СвПосрДеят>
//              <СвПосрДеят НомСчФОтПрод="765" ДатаСчФОтПрод="04.03.2015" ОКВ="643" РазСтКСчФУм="200.00" РазСтКСчФУв="210.00" РазНДСКСчФУм="220" РазНДСКСчФУв="230">
//                  <СвПрод>
//                      <СведЮЛ ИННЮЛ="5042063636" КПП="504201001" />
//                  </СвПрод>
//              </СвПосрДеят>
//        </ЖУчВыстСчФСтр>
//    </ЖУчВыстСчФ>
//    </Документ>
//    </Файл>

    public String createXml(List<EntityRazdel10> entityRazdel10s, String fileName) {

        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("<?xml version=\"1.0\" encoding=\"windows-1251\" ?> \n" +
                "<Файл xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" ИдФайл=\"").append(fileName).append("\" ВерсПрог=\"Налогоплательщик ЮЛ 4.40.4\" ВерсФорм=\"5.04\">\n" +
                "<Документ Индекс=\"0000100\" НомКорр=\"0\">\n" +
                "<ЖУчВыстСчФ>\n");

        int i = 1;
        for (EntityRazdel10 e : entityRazdel10s) {

            stringBuilder.append("<ЖУчВыстСчФСтр НомерПор=\"").append(i);

            if (e.getDataVyst() != null) {
                stringBuilder.append("\" ДатаВыст=\"").append(e.getDataVyst());
            }

            if (e.getNomSchFPokup() != null) {
                stringBuilder.append("\" НомСчФПрод=\"").append(e.getNomSchFPokup());
            }

            if (e.getDataSchFPokup() != null) {
                stringBuilder.append("\" ДатаСчФПрод=\"").append(e.getDataSchFPokup());
            }

            if (e.getNomIsprSchF() != null) {
                stringBuilder.append("\" НомИспрСчФ=\"").append(e.getNomIsprSchF());
            }

            if (e.getDataIsprSchF() != null) {
                stringBuilder.append("\" ДатаИспрСчФ=\"").append(e.getDataIsprSchF());
            }

            if (e.getNomIsprKSchF() != null) {
                stringBuilder.append("\" НомКСчФПрод=\"").append(e.getNomIsprKSchF());
            }

            if (e.getDataKSchDPokup() != null) {
                stringBuilder.append("\" ДатаКСчФПрод=\"").append(e.getDataKSchDPokup());
            }

            if (e.getNomIsprKSchF() != null) {
                stringBuilder.append("\" НомИспрКСчФ=\"").append(e.getNomIsprKSchF());
            }

            if (e.getDataIsprKSchf() != null) {
                stringBuilder.append("\" ДатаИспрКСчФ=\"").append(e.getDataIsprKSchf());
            }
            stringBuilder.append("\">\n");

            if (e.getKodVidOper() != null) {
                stringBuilder.append("<КодВидОпер>").append(e.getKodVidOper()).append("</КодВидОпер>\n");
            }
            stringBuilder.append("<СвПокуп>\n");

            if (e.getInnPokup() != null && e.getInnPokup().length() == 10) {
                stringBuilder.append("<СведЮЛ ИННЮЛ=\"").append(e.getInnPokup());
                if (e.getKppPokup() != null) {
                    stringBuilder.append("\" КПП=\"").append(e.getKppPokup());
                }
            } else if (e.getInnPokup() != null && e.getInnPokup().length() == 12) {
                stringBuilder.append("<СведИП ИННФЛ=\"").append(e.getInnPokup());
            }


                stringBuilder.append("\" />\n");
                stringBuilder.append("</СвПокуп>\n");


//            System.out.println(stringBuilder.toString());

            try {
                for (SubEntities10Prod s : e.getProds()) {
                    stringBuilder.append("<СвПосрДеят НомСчФОтПрод=\"").append(s.getNomSchFOtProdProd());

                    if (s.getDataSchFOtProdProd() != null) {
                        stringBuilder.append("\" ДатаСчФОтПрод=\"").append(s.getDataSchFOtProdProd());
                    }

                    if (s.getOkvProd() != null) {
                        stringBuilder.append("\" ОКВ=\"").append(s.getOkvProd());
                    }

                    if (s.getStoimTovSchFVs() != null) {
                        stringBuilder.append("\" СтоимТовСчФВс=\"").append(s.getStoimTovSchFVs());
                    }

                    if (s.getSumNDSSchF() != null) {
                        stringBuilder.append("\" СумНДССчФ=\"").append(s.getSumNDSSchF());
                    }

                    if (s.getRazStKSchFUmProd() != null) {
                        stringBuilder.append("\" РазСтКСчФУм=\"").append(s.getRazStKSchFUmProd());
                    }

                    if (s.getRazStKSchAUvProd() != null) {
                        stringBuilder.append("\" РазСтКСчФУв=\"").append(s.getRazStKSchAUvProd());
                    }

                    if (s.getRazNDSKSchFUmProd() != null) {
                        stringBuilder.append("\" РазНДСКСчФУм=\"").append(s.getRazNDSKSchFUmProd());
                    }

                    if (s.getRazNDSKSchFUvProd() != null) {
                        stringBuilder.append("\" РазНДСКСчФУв=\"").append(s.getRazNDSKSchFUvProd());
                    }

                    stringBuilder.append("\">\n");

                    stringBuilder.append("<СвПрод>\n");

                    if (s.getInnProd() != null && s.getInnProd().length() == 10) {
                        stringBuilder.append("<СведЮЛ ИННЮЛ=\"").append(s.getInnProd());
                        if (s.getKppProd() != null) {
                            stringBuilder.append("\" КПП=\"").append(s.getKppProd());
                        }
                    }

                    if (s.getInnProd() != null && s.getInnProd().length() == 12) {
                        stringBuilder.append("<СведИП ИННФЛ=\"").append(s.getInnProd());
                    }

                    stringBuilder.append("\" />\n");

                    stringBuilder.append("</СвПрод>\n");
                    stringBuilder.append("</СвПосрДеят>\n");
                    //stringBuilder.append("\n");
                }
            } catch (Exception e1) {
//                e1.printStackTrace();
            }

            stringBuilder.append("</ЖУчВыстСчФСтр>\n");
            stringBuilder.append("\n");
//            System.out.println(stringBuilder.toString());
            i++;
        }

        stringBuilder.append("</ЖУчВыстСчФ>\n");
        stringBuilder.append("</Документ>\n");
        stringBuilder.append("</Файл>\n");

        return stringBuilder.toString();
    }

}