package xml;

import entity.EntityRazdel10;
import entity.EntityRazdel11;
import entity.subEntities.SubEntities10Prod;
import org.apache.log4j.Logger;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Sheet;
import tests.ParamTesting;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

public class ReaderExcel {

    static final Logger logger = Logger.getLogger(ReaderExcel.class);

    public List<EntityRazdel10> parseRazdel10(String name) {

        List<EntityRazdel10> entityRazdel10s = new ArrayList<EntityRazdel10>();
        InputStream in;
        HSSFWorkbook wb = null;
        try {
            in = new FileInputStream(name);
            wb = new HSSFWorkbook(in);
        } catch (IOException e) {
            e.printStackTrace();
        }
        Sheet sheet = wb.getSheetAt(2);
        List<ArrayList> arrayLists = new ReadOneList().read(sheet);

        String result = null;
        for (ArrayList arrayList : arrayLists) {
            int j = 0;
            for (Object o : arrayList) {
                if (o != null) {
                    result += j + "#" + o + "\t";
                }
                j++;
            }
            result += "\n";
        }
        System.out.println(result);
// Записываем номена накладных в set коллекцию, получаем список уникальных номеров накладных
        Set<String> stringSet = new TreeSet<String>();
        String[] str;
        String temp = null;
        for (List array : arrayLists) {
            try {
                temp = array.get(4).toString();
                str = temp.split("\\s+");
                if (str.length > 1) {
                    stringSet.add(str[0]);
                }
            } catch (NullPointerException e) {
                e.printStackTrace();
            }
//            System.out.println();
        }
////////////////////////////////////////////////

//0#2	1#04	4#7 от 05.01.2015	6#ООО "СВИФ-Плюс"	8#2721198255/272101001
// 9#ООО "Транспортно-Логистический Центр"	11#7811419211/780601001	13#25 от 05.01.2015
// 14#российский рубль,643	17#90000	19#76271.19	22#13728.81

        ArrayList<ArrayList<ArrayList>> lists = new ArrayList<ArrayList<ArrayList>>();
        Integer i = 0;
        for (String s : stringSet) {
            lists.add(new ArrayList<ArrayList>());
            int j = 0;
            for (ArrayList arrayList : arrayLists) {
                temp = arrayList.get(4).toString();
                str = temp.split("\\s+");

                if (s.equals(str[0])) {
                    lists.get(i).add(arrayLists.get(j));
                }
                j++;
            }
            i++;
        }

///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
//        for (ArrayList<ArrayList> list : lists) {
////            System.out.println(list.get(ind).get(4).toString());
//
//        }

        for (ArrayList<ArrayList> list : lists) {
            entityRazdel10s.add(new EntityRazdel10());

            int ind = lists.indexOf(list);
//            String tempData = list.get(ind).get(4).toString();
            String tempData = list.get(0).get(4).toString();
            String[] tempStr = tempData.split("от");
            if (tempStr.length > 1) {
                entityRazdel10s.get(ind).setDataVyst(tempStr[1].trim());
                entityRazdel10s.get(ind).setNomSchFPokup(tempStr[0].trim());
                entityRazdel10s.get(ind).setDataSchFPokup(tempStr[1].trim());
            }

            tempData = "";
            System.out.println(list.get(0).get(1).toString().trim() + "-> " + tempStr[0].trim());
            entityRazdel10s.get(ind).setKodVidOper(list.get(0).get(1).toString());

            try {
                tempStr = list.get(0).get(32).toString().split("/");
            } catch (NullPointerException e) {
                System.err.println(list.get(0).get(4));
            }
            if (tempStr.length > 1) {
                if (tempStr[0].isEmpty()) {
                    entityRazdel10s.get(ind).setInnPokup("");
                } else
                    entityRazdel10s.get(ind).setInnPokup(tempStr[0].trim());

                if (tempStr[1].trim().isEmpty()) {
                    entityRazdel10s.get(ind).setKppPokup("");
                } else
                    entityRazdel10s.get(ind).setKppPokup(tempStr[1].trim());
            } else if (tempStr.length == 1) {
                if (tempStr[0].trim().isEmpty()) {
                    entityRazdel10s.get(ind).setInnPokup("");
                } else
                    entityRazdel10s.get(ind).setInnPokup(tempStr[0].trim());
            }

            tempData = "";

//        0#7.0 1#04	4#2015691    12.01.2015		30#ООО "Осмин"	32#6658076497 / 667201001	33#ООО "Инком"
// 35#7811552647/781101001	37#32 от 12.01.2015	38#643.0	41#66947.91	43#56735.52	44#0.0	45#0.0	46#10212.39	47#0.0	48#0.0
            int count2 = 0;
            for (ArrayList a : list) {
//                System.out.println();
                entityRazdel10s.get(ind).getProds().add(new SubEntities10Prod());

                String[] rtsTemp = a.get(35).toString().split("/");

                entityRazdel10s.get(ind).getProds().get(count2).setInnProd(rtsTemp[0]);
                entityRazdel10s.get(ind).getProds().get(count2).setKppProd(rtsTemp[1]);

                tempData = a.get(37).toString();
                String[] rtsData = tempData.split("от");

                if (rtsData.length > 1) {
                    entityRazdel10s.get(ind).getProds().get(count2).setNomSchFOtProdProd(rtsData[0].trim());
                    entityRazdel10s.get(ind).getProds().get(count2).setDataSchFOtProdProd(rtsData[1].trim());
                }
                temp = "";

                String h = a.get(38).toString();
                String[] j = h.split(",");
                entityRazdel10s.get(ind).getProds().get(count2).setOkvProd(j[1]);
                entityRazdel10s.get(ind).getProds().get(count2).setStoimTovSchFVs(a.get(41).toString());
                entityRazdel10s.get(ind).getProds().get(count2).setSumNDSSchF(a.get(46).toString());
                count2++;
            }
        }

        arrayLists = null;

        i = 0;
        return entityRazdel10s;
    }

    public List<EntityRazdel11> parseRazdel11(String name) {

        StringBuilder result = new StringBuilder();
        List<EntityRazdel11> entityRazdel11s = new ArrayList<EntityRazdel11>();
        InputStream in;
        HSSFWorkbook wb = null;
        try {
            in = new FileInputStream(name);
            wb = new HSSFWorkbook(in);
        } catch (IOException e) {
            e.printStackTrace();
        }

        Sheet sheet = wb.getSheetAt(0);
        List<ArrayList> listOfRows = new ReadOneList().read(sheet);

        //Удалить первые 3 строки из коллекции
        for (int del = 0; del < 3; del++) {
            listOfRows.remove(0);
        }
        for (ArrayList row : listOfRows) {
            entityRazdel11s.add(new EntityRazdel11());
            int j = 0;
            for (Object cell : row) {
                if (cell != null) {
                    result.append(row.indexOf(cell) + "#" + cell + "\t");
                    switch (j) {

                        //Код вида операции
                        case 0:
                            entityRazdel11s.get(listOfRows.indexOf(row)).setKodVidOper(cell.toString().trim());
                            break;

                        //Дата получения
                        case 1:
                            entityRazdel11s.get(listOfRows.indexOf(row)).setDataPoluch(cell.toString().trim());
                            break;
                        //номер счёт-фактуры
                        case 2:
                            entityRazdel11s.get(listOfRows.indexOf(row)).setNomSchFProd(cell.toString().trim());
                            break;


                        // дата счета-фактуры
                        case 3:
                            entityRazdel11s.get(listOfRows.indexOf(row)).setDataSchFProd(cell.toString().trim());
                            break;


                        case 4:
                            break;

                        // дата исправления счета-фактуры
                        case 5:
                            break;
                        case 6:
                            break;

                        // дата корректировочниго счета-фактуры
                        case 7:
                            break;
                        case 8:
                            break;

                        //дата исправления корректировочного счета-фактуры
                        case 9:
                            break;

                        //Наименование продавца
                        case 10:
                            break;

                        //инн продавца
                        case 11:
                            entityRazdel11s.get(listOfRows.indexOf(row)).setInnProd(cell.toString().trim());
                            break;

                        //кпп продавца
                        case 12:
                            entityRazdel11s.get(listOfRows.indexOf(row)).setKppProd(cell.toString().trim());
                            break;

                        //Наименование  субкомиссионера
                        case 13:
                            break;

                        //инн  субкомиссионера
                        case 14:
                            entityRazdel11s.get(listOfRows.indexOf(row)).setInnKomis(cell.toString().trim());
                            break;

                        //кпп субкомиссионера
                        case 15:
                            entityRazdel11s.get(listOfRows.indexOf(row)).setKppKomis(cell.toString().trim());
                            break;

                        //номер документа
                        case 16:
                            break;

                        //дата документа,
                        case 17:
                            break;

                        //Наименование валюты
                        case 18:
                            break;

                        //код валюты
                        case 19:
                            entityRazdel11s.get(listOfRows.indexOf(row)).setOkv(cell.toString().trim());
                            break;

                        //Стоимость продаж по разница стоимости по  счета-фактуре всего
                        case 20:
                            entityRazdel11s.get(listOfRows.indexOf(row)).setStoimTovSchFVs(cell.toString().trim());
                            break;

                        //НДС по счет-фактуре
                        case 21:
                            entityRazdel11s.get(listOfRows.indexOf(row)).setSumNDSSchF(cell.toString().trim());
                            break;

                        //Разница стоимости с учетом НДС по корректировочному счету -  фактуре УМЕНЬНЕНИЕ
                        case 22:
                            break;

                        //Разница стоимости с учетом НДС по корректировочному счету -  фактуре УВЕЛИЧЕНИЕ
                        case 23:
                            break;

                        //Разница НДС по корректировочному счету-фактуре УМЕНЬШЕНИЕ
                        case 24:
                            break;

                        //Разница НДС по корректировочному счету-фактуре УВЕЛИЧЕНИЕ
                        case 25:
                            break;

//                        case 1:
//                            if (cell.toString().trim().contains(".")) {
//                                String st[];
//                                st = cell.toString().trim().split("\\.");
//                                entityRazdel11s.get(listOfRows.indexOf(row)).setKodVidOper(st[0]);
//                            } else {
//                                entityRazdel11s.get(listOfRows.indexOf(row)).setKodVidOper(cell.toString().trim());
//                            }
//                            break;
//                        case 2:
//                            entityRazdel11s.get(listOfRows.indexOf(row)).setNomSchFProd(cell.toString().trim());
//                            break;
//                        case 3:
//                            entityRazdel11s.get(listOfRows.indexOf(row)).setDataSchFProd(cell.toString().trim());
//                            break;
//                        case 13:
//                            String str[];
//                            str = cell.toString().trim().split("/");
//                            if (str.length > 0) {
//                                entityRazdel11s.get(listOfRows.indexOf(row)).setInnProd(str[0].trim());
//                            }
//                            if (str.length > 1) {
//                                entityRazdel11s.get(listOfRows.indexOf(row)).setKppProd(str[1].trim());
//                            }
//                            break;
//                        case 16:
//                            if (cell.toString().trim().contains(".")) {
//                                String st[];
//                                st = cell.toString().trim().split("\\.");
//                                entityRazdel11s.get(listOfRows.indexOf(row)).setKodVidSd(st[0]);
//                            } else {
//                                entityRazdel11s.get(listOfRows.indexOf(row)).setKodVidSd(cell.toString().trim());
//                            }
//                            break;
//                        case 18:
//                            String okv = cell.toString().trim().replaceAll("\\D", "");
//                            entityRazdel11s.get(listOfRows.indexOf(row)).setOkv(okv);
//                            break;
//                        case 19:
//                            entityRazdel11s.get(listOfRows.indexOf(row)).setStoimTovSchFVs(cell.toString().trim());
//                            break;
//                        case 20:
//                            entityRazdel11s.get(listOfRows.indexOf(row)).setSumNDSSchF(cell.toString().trim());
//                            break;
                    }
                }
//                0#12.0	1#04	2#34/15	3#13.01.2015	4#   	5#  	12#ООО "ЕвроСервис"	13#784401001/7814436744
// 16#1.0	18#643  Российский рубль	19#106000.0	20#16169.49
                j++;
            }
            result.append("\n");
        }
        System.out.println(result);
        for (EntityRazdel11 entityRazdel11 : entityRazdel11s) {
            if (entityRazdel11.getInnProd() != null && !ParamTesting.innTesting(entityRazdel11.getInnProd())) {
                logger.info("Раздел-11. Счёт-фактура № " + entityRazdel11.getNomSchFProd());
                logger.info("ИНН продавца некорректный ---> " + entityRazdel11.getInnProd());
                logger.info(" ==========================================");
            }

            if (entityRazdel11.getKppProd() != null && !ParamTesting.kppTesting(entityRazdel11.getKppProd(), entityRazdel11.getKppProd())) {
                logger.info("Раздел-11. Счёт-фактура № " + entityRazdel11.getNomSchFProd());
                logger.info("КПП продавца некорректный ---> " + entityRazdel11.getKppProd());
                logger.info(" ==========================================");
            }

            if (entityRazdel11.getStoimTovSchFVs() != null && !ParamTesting.check2NumberAfterDot(entityRazdel11.getStoimTovSchFVs())) {
                logger.info("Раздел-9. Счёт-фактура № " + entityRazdel11.getNomSchFProd());
                logger.info("В стоитости более двух знаком после запятой");
                logger.info("==========================================");
            }

            if (entityRazdel11.getSumNDSSchF() != null && !ParamTesting.check2NumberAfterDot(entityRazdel11.getSumNDSSchF())) {
                logger.info("Раздел-9. Счёт-фактура № " + entityRazdel11.getNomSchFProd());
                logger.info("В стоитости более двух знаком после запятой");
                logger.info("==========================================");
            }
        }
        return entityRazdel11s;
    }
}