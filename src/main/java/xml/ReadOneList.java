package xml;

import org.apache.poi.hssf.usermodel.HSSFDateUtil;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;

import java.text.SimpleDateFormat;
import java.util.*;


public class ReadOneList {

    public List<ArrayList> read(Sheet sheet) {

        Iterator<Row> it = sheet.iterator();
        int i = 0;
//        int j = 0;

        List<ArrayList> allRows = new ArrayList<ArrayList>();
        while (it.hasNext()) {
            Row row = it.next();
            Iterator<Cell> cells = row.iterator();

            List<String> rowFromTable = new ArrayList();
            while (cells.hasNext()) {
                Cell cell = cells.next();

                int cellType = cell.getCellType();
                switch (cellType) {
                    case Cell.CELL_TYPE_STRING:
                        rowFromTable.add(cell.getStringCellValue().trim());
                        break;
                    case Cell.CELL_TYPE_NUMERIC:
                        double d = cell.getNumericCellValue();


                        String temp = String.valueOf(cell);
                        Calendar cal = new GregorianCalendar(TimeZone.getDefault(), Locale.US);
                        // test if a date!
                        if (HSSFDateUtil.isCellDateFormatted(cell)) {
                            cal.setTime(HSSFDateUtil.getJavaDate(d));
                            SimpleDateFormat format1 = new SimpleDateFormat("dd.MM.yyyy");
                            String formatted = format1.format(cal.getTime());
                            rowFromTable.add(formatted);
                        } else {
                            cell.setCellType(Cell.CELL_TYPE_STRING);
                            rowFromTable.add(cell.getStringCellValue().trim());
                        }
                        break;

                    case Cell.CELL_TYPE_FORMULA:
                        System.out.println(i+1);//TODO показывает строки с формулами
                        rowFromTable.add(String.valueOf(cell.getNumericCellValue()));
                        break;
                    default:
                        rowFromTable.add(null);
                        break;
                }
//                j++;
            }
            allRows.add((ArrayList)rowFromTable);
            i++;
            rowFromTable = null;
        }

        return allRows;
    }
}
