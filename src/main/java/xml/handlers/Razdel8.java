package xml.handlers;

import ejb.MakeAllReports;
import entity.EntityRazdel8;
import org.apache.log4j.Logger;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Sheet;
import tests.ParamTesting;
import xml.ReadOneList;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by konstantin on 23.04.15.
 */
public class Razdel8 {

	static final Logger logger = Logger.getLogger(Razdel8.class);

	public List<EntityRazdel8> parseRazdel8(InputStream in) {

		List<EntityRazdel8> entityRazdel8s = new ArrayList<EntityRazdel8>();
		// InputStream in;
		HSSFWorkbook wb = null;
		try {
			// in = new FileInputStream(name);
			wb = new HSSFWorkbook(in);
		} catch (IOException e) {
			e.printStackTrace();
		}

		Integer i = 0;
		Sheet sheet = wb.getSheetAt(0);
		List<ArrayList> arrayLists = new ReadOneList().read(sheet);

		// Удалить первые 2 строки из коллекции
		for (int del = 0; del < 2; del++) {
			arrayLists.remove(0);
		}

		for (List array : arrayLists) {

			if (array.size() > 10) {
				entityRazdel8s.add(new EntityRazdel8());
				String result = "";
				int j = 0;
				for (Object s : array) {

					result += j + "#" + s + "\t";
					if (s != null) {

						String temp = (String) s;
						temp = temp.replaceAll("\\\"", "").trim();

						try {
							switch (j) {
							// Код вида операции (<КодВидОпер>01</КодВидОпер>)
							case 0:
								entityRazdel8s.get(i).setKodVidOper(temp);
								break;

							// номер счета-фактуры продавца(НомСчФПрод)
							case 1:
								entityRazdel8s.get(i).setNomSchFProd(temp);
								break;

							// дата счета-фактуры продавца(ДатаСчФПрод)
							case 2:
								entityRazdel8s.get(i).setDataSchFProd(temp);
								break;

							//  номер исправления счета-фактуры продавца(НомИспрСчФ)
							case 3:
								entityRazdel8s.get(i).setNomIspSchF(temp);
								break;

							//  дата исправления счета-фактуры продавца(ДатаИспрСчФ)
							case 4:
								entityRazdel8s.get(i).setDataIsprSchF(temp);
								break;

							//  номер корректировочниго счета-фактуры продавца(НомКСчФПрод)
							case 5:
								entityRazdel8s.get(i).setNomKSchFProd(temp);
								break;

							//  дата корректировочниго счета-фактуры продавца(ДатаКСчФПрод)
							case 6:
								entityRazdel8s.get(i).setDataKSchFProd(temp);
								break;

							// номер исправления корректировочного счета-фактуры
							// продавца(НомИспрКСчФ)
							case 7:
								entityRazdel8s.get(i).setNomIsprKSchF(temp);
								break;

							// дата исправления корректировочного счета-фактуры
							// продавца(ДатаИспрКСчФ)
							case 8:
								entityRazdel8s.get(i).setDataIsprKSchF(temp);
								break;

							// номер документа, подтверждающего уплату налога (НомДокПдтвУпл)
							case 9:
								entityRazdel8s.get(i).setNomDokPdtvUpl(temp);
								break;

							// дата документа, подтверждающего уплату налога (ДатаДокПдтвУпл)
							case 10:
								entityRazdel8s.get(i).setDataDokPdtvUpl(temp);
								break;

							// Дата принятия на учет товаров (работ, услуг),
							// имущественных прав (ДатаУчТов)
							case 11:
								break;

							//  Код и название продовца
							case 12:
								break;

							// ИНН продавца(ИННЮЛ)
							case 13:
								entityRazdel8s.get(i).setInnProd(temp);
								break;

							// КПП продавца(КПП)
							case 14:
								entityRazdel8s.get(i).setKppProd(temp);
								break;

							// Наименование посредника
							case 15:
								break;

							// инн посредника
							case 16:
								entityRazdel8s.get(i).setInnPosr(temp);
								break;

							// кпп Посредника
							case 17:
								entityRazdel8s.get(i).setKppPosr(temp);
								break;

							// Код вида сделки(КодВидСд)
							case 18:
								break;

							// Номер таможенной декларации(НомТД)
							case 19:
								entityRazdel8s.get(i).setNomTD(temp.trim().replaceAll("\\s+", " "));
								break;

							// Наименование валюты
							case 20:
								break;

							// код валюты(ОКВ)
							case 21:
								entityRazdel8s.get(i).setOkv(temp);
								break;

							// Стоимость покупок по счету-фактуре, разница
							// стоимости по корректировочному счет-фактуре
							// (включая НДС) в валюте счета- фактуры (СтоимПокупВ)
							case 22:
								entityRazdel8s.get(i).setStoimPokupV(temp);
								break;

							// Сумма НДС по счету-фактуре, разница суммы НДС по
							// корректировочному счету-фактуре, принимаемая к
							// вычету, в рублях и копейках (СумНДСВыч)
							case 23:
								entityRazdel8s.get(i).setSumNdsVych(temp);
								break;
							}
						} catch (NumberFormatException e) {
							e.printStackTrace();
						}
					}
					j++;
				}
				// System.out.println(result);
				result = null;
			}
			i++;
		}
		arrayLists = null;

		i = 0;

		//////////////////////////////////////////////////////
		/// Тестирование данных в счёт-фактурах
		//////////////////////////////////////////////////////
		for (EntityRazdel8 entityRazdel8 : entityRazdel8s) {

			if (entityRazdel8.getNomIspSchF() != null
					&& !ParamTesting.nomerIstrSchFTesting(entityRazdel8.getNomIspSchF())) {
				MakeAllReports.errors.append("Раздел-8. Счёт-фактура № ").append(entityRazdel8.getNomSchFProd())
						.append("<br />")
						.append("Номер исправления <b>")
						.append(entityRazdel8.getNomIspSchF())
						.append("</b> счёт-фактуры не корректный<br /> ==========================================<br />");
				logger.info("Раздел-8. Счёт-фактура № " + entityRazdel8.getNomSchFProd());
				logger.info("Номер исправления счёт-фактуры не корректный");
				logger.info(" ==========================================");
			}

			if (entityRazdel8.getInnProd() != null && !ParamTesting.innTesting(entityRazdel8.getInnProd())) {
				MakeAllReports.errors.append("<br />").append("Раздел-8. Счёт-фактура № ")
						.append(entityRazdel8.getNomSchFProd()).append("<br />")
						.append("ИНН продавца <b>")
						.append(entityRazdel8.getInnProd())
						.append("</b> некорректный<br /> ==========================================");
				logger.info("Раздел-8. Счёт-фактура № " + entityRazdel8.getNomSchFProd());
				logger.info("ИНН продавца некорректный");
				logger.info(" ==========================================");
			}

			if (entityRazdel8.getInnPosr() != null && !ParamTesting.innTesting(entityRazdel8.getInnPosr())) {
				MakeAllReports.errors.append("<br />").append("Раздел-8. Счёт-фактура № ")
						.append(entityRazdel8.getNomSchFProd()).append("<br />")
						.append("ИНН посредника <b>")
						.append(entityRazdel8.getInnPosr())
						.append("</b> некорректный<br /> ==========================================");
				logger.info("Раздел-8. Счёт-фактура № " + entityRazdel8.getNomSchFProd());
				logger.info("ИНН посредника некорректный");
				logger.info(" ==========================================");
			}

			if (entityRazdel8.getKppProd() != null && !ParamTesting.kppTesting(entityRazdel8.getKppProd(), entityRazdel8.getInnProd())) {
				MakeAllReports.errors.append("<br />").append("Раздел-8. Счёт-фактура № ")
						.append(entityRazdel8.getNomSchFProd()).append("<br />")
						.append("КПП продавца <b>")
						.append(entityRazdel8.getKppProd())
						.append("</b> некорректный<br /> ==========================================");
				logger.info("Раздел-8. Счёт-фактура № " + entityRazdel8.getNomSchFProd());
				logger.info("КПП продавца некорректный" + "---> " + entityRazdel8.getKppProd());
				logger.info(" ==========================================");
			}

			if (entityRazdel8.getKppPosr() != null && !ParamTesting.kppTesting(entityRazdel8.getKppPosr(), entityRazdel8.getInnPosr())) {
				MakeAllReports.errors.append("<br />").append("Раздел-8. Счёт-фактура № ")
						.append(entityRazdel8.getNomSchFProd()).append("<br />")
						.append("КПП посредника <b>")
						.append(entityRazdel8.getKppPosr())
						.append("</b> некорректный<br /> ==========================================");
				logger.info("Раздел-8. Счёт-фактура № " + entityRazdel8.getNomSchFProd());
				logger.info("КПП посредника некорректный");
				logger.info("==========================================");
			}

			if (entityRazdel8.getStoimPokupV() != null
					&& !ParamTesting.check2NumberAfterDot(entityRazdel8.getStoimPokupV())) {
				MakeAllReports.errors.append("<br />").append("Раздел-8. Счёт-фактура № ")
						.append(entityRazdel8.getNomSchFProd()).append("<br />")
						.append("В стоитости <b>")
						.append(entityRazdel8.getStoimPokupV())
						.append("</b> более двух знаком после запятой<br /> ==========================================");
				logger.info("Раздел-8. Счёт-фактура № " + entityRazdel8.getNomSchFProd());
				logger.info("В стоитости более двух знаком после запятой");
				logger.info("==========================================");
			}

			if (entityRazdel8.getSumNdsVych() != null
					&& !ParamTesting.check2NumberAfterDot(entityRazdel8.getSumNdsVych())) {
				MakeAllReports.errors.append("<br />").append("Раздел-8. Счёт-фактура № ")
						.append(entityRazdel8.getNomSchFProd()).append("<br />")
						.append("В сумме НДС <b>")
						.append(entityRazdel8.getSumNdsVych())
						.append("</b> более двух знаком после запятой<br /> ==========================================");
				logger.info("Раздел-8. Счёт-фактура № " + entityRazdel8.getNomSchFProd());
				logger.info("В сумме НДС более двух знаком после запятой");
				logger.info("==========================================");
			}

			if (entityRazdel8.getNomTD() != null && !ParamTesting.checkNomTD(entityRazdel8.getNomTD())) {
				MakeAllReports.errors.append("<br />").append("Раздел-8. Счёт-фактура № ")
						.append(entityRazdel8.getNomSchFProd()).append("<br />")
						.append("Номер ТД <b>")
						.append(entityRazdel8.getNomTD())
						.append("</b> больше 29 символов<br /> ==========================================");
				logger.info("Раздел-8. Счёт-фактура № " + entityRazdel8.getNomSchFProd());
				logger.info("Номер ТД больше 29 символов");
				logger.info("==========================================");
			}

			if (entityRazdel8.getKodVidOper() != null && !entityRazdel8.getKodVidOper().isEmpty()
					&& !ParamTesting.checkKodVidOper(entityRazdel8.getKodVidOper())) {
				MakeAllReports.errors.append("<br />").append("Раздел-8. Счёт-фактура № ")
						.append(entityRazdel8.getNomSchFProd()).append("<br />")
						.append("Код вида операции <b>")
						.append(entityRazdel8.getKodVidOper())
						.append("</b> должен состоять из двух символов")
						.append(entityRazdel8.getKodVidOper())
						.append("<br /> ==========================================");
				logger.info("Раздел-8. Счёт-фактура № " + entityRazdel8.getNomSchFProd());
				logger.info(
						"Код вида операции должен состоять из двух символов" + "--->" + entityRazdel8.getKodVidOper());
				logger.info("==========================================");
			}
		}

		return entityRazdel8s;
	}
}
