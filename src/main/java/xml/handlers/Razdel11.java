package xml.handlers;

import ejb.MakeAllReports;
import entity.EntityRazdel11;
import org.apache.log4j.Logger;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Sheet;
import tests.ParamTesting;
import xml.ReadOneList;

import java.io.ByteArrayInputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

public class Razdel11 {

	static final Logger logger = Logger.getLogger(Razdel11.class);

	public List<EntityRazdel11> parseRazdel11(InputStream in) {

		StringBuilder result = new StringBuilder();
		List<EntityRazdel11> entityRazdel11s = new ArrayList<EntityRazdel11>();
		// InputStream in;
		HSSFWorkbook wb = null;
		try {
			// in = new FileInputStream(fileName);
			// in = new ByteArrayInputStream(file.getBytes());
			wb = new HSSFWorkbook(in);
		} catch (IOException e) {
			e.printStackTrace();
		}

		Sheet sheet = wb.getSheetAt(0);
		List<ArrayList> listOfRows = new ReadOneList().read(sheet);

		// Удалить первые 2 строки из коллекции
		for (int del = 0; del < 2; del++) {
			listOfRows.remove(0);
		}

		for (ArrayList row : listOfRows) {
			entityRazdel11s.add(new EntityRazdel11());
			int j = 0;

			for (Object cell : row) {
				if (cell != null) {
					result.append(row.indexOf(cell) + "#" + cell + "\t");

					switch (j) {

					// Код вида операции
					case 0:
						entityRazdel11s.get(listOfRows.indexOf(row)).setKodVidOper(cell.toString().trim());
						break;

					// Дата получения(ДатаПолуч)
					case 1:
						entityRazdel11s.get(listOfRows.indexOf(row)).setDataPoluch(cell.toString().trim());
						break;
					// номер счёт-фактуры(НомСчФПрод)
					case 2:
						entityRazdel11s.get(listOfRows.indexOf(row)).setNomSchFProd(cell.toString().trim());
						break;
					// дата счета-фактуры(ДатаСчФПрод)
					case 3:
						entityRazdel11s.get(listOfRows.indexOf(row)).setDataSchFProd(cell.toString().trim());
						break;

					case 4:
						break;

					//  дата исправления счета-фактуры(НомИспрСчФ)
					case 5:
						break;
					case 6:
						break;

					//  дата корректировочниго счета-фактуры(ДатаКСчФПрод)
					case 7:
						break;

					//Номер корректировочного счета-фактуры(НомКСчФПрод)
					case 8:
						break;

					// дата исправления корректировочного счета-фактуры(ДатаИспрКСчФ)
					case 9:
						break;

					// Наименование продавца
					case 10:
						break;

					// инн продавца (<СвПрод> <СведЮЛ ИННЮЛ=)
					case 11:
						entityRazdel11s.get(listOfRows.indexOf(row)).setInnProd(cell.toString().trim());
						break;

					// кпп продавца (КПП)
					case 12:
						entityRazdel11s.get(listOfRows.indexOf(row)).setKppProd(cell.toString().trim());
						break;

					// Наименование субкомиссионера
					case 13:
						break;

					// инн  субкомиссионера
					case 14:
						entityRazdel11s.get(listOfRows.indexOf(row)).setInnKomis(cell.toString().trim());
						break;

					// кпп субкомиссионера
					case 15:
						entityRazdel11s.get(listOfRows.indexOf(row)).setKppKomis(cell.toString().trim());
						break;

					// Наименование валюты
					case 16:
						break;

					// код валюты(ОКВ)
					case 17:
						entityRazdel11s.get(listOfRows.indexOf(row)).setOkv(cell.toString().trim());
						break;

					// Код вида сделки(КодВидСд)
					case 18:
						entityRazdel11s.get(listOfRows.indexOf(row)).setKodVidSd(cell.toString().trim());
						break;

					// Стоимость продаж (работ, услуг) имущественных прав по
					// счета-фактуре всего(СтоимТовСчФВс)
					case 19:
						entityRazdel11s.get(listOfRows.indexOf(row)).setStoimTovSchFVs(cell.toString().trim());
						break;

					// В том числе сумма НДС по счету-фактуре(СумНДССчФ)
					case 20:
						entityRazdel11s.get(listOfRows.indexOf(row)).setSumNDSSchF(cell.toString().trim());
						break;

					// Разница стоимости с учетом НДС по корректировочному счету
					// - фактуре УМЕНЬНЕНИЕ
					case 21:
						break;

					// Разница стоимости с учетом НДС по корректировочному счету
					// - фактуре УВЕЛИЧЕНИЕ
					case 22:
						break;

					// Разница НДС по корректировочному счету-фактуре УМЕНЬШЕНИЕ
					case 23:
						break;

					// Разница НДС по корректировочному счету-фактуре УВЕЛИЧЕНИЕ
					case 24:
						break;
					}
				}
				j++;
			}
			result.append("\n");
		}
		// System.out.println(result);
		for (EntityRazdel11 entityRazdel11 : entityRazdel11s) {
			if (entityRazdel11.getInnProd() != null && !ParamTesting.innTesting(entityRazdel11.getInnProd())) {
				MakeAllReports.errors.append("<br />").append("Раздел-8. Счёт-фактура № ")
						.append(entityRazdel11.getNomSchFProd()).append("<br />").append("ИНН продавца некорректный ")
						.append(entityRazdel11.getInnProd()).append("<br />")
						.append(" ==========================================");
				logger.info("Раздел-11. Счёт-фактура № " + entityRazdel11.getNomSchFProd());
				logger.info("ИНН продавца некорректный ---> " + entityRazdel11.getInnProd());
				logger.info(" ==========================================");
			}

			if (entityRazdel11.getKppProd() != null
					&& !ParamTesting.kppTesting(entityRazdel11.getKppProd(), entityRazdel11.getInnProd())) {
				MakeAllReports.errors.append("<br />").append("Раздел-8. Счёт-фактура № ")
						.append(entityRazdel11.getNomSchFProd()).append("<br />").append("КПП продавца некорректный ")
						.append(entityRazdel11.getKppProd()).append("<br />")
						.append(" ==========================================");
				logger.info("Раздел-11. Счёт-фактура № " + entityRazdel11.getNomSchFProd());
				logger.info("КПП продавца некорректный ---> " + entityRazdel11.getKppProd());
				logger.info(" ==========================================");
			}

			if (entityRazdel11.getStoimTovSchFVs() != null
					&& !ParamTesting.check2NumberAfterDot(entityRazdel11.getStoimTovSchFVs())) {
				MakeAllReports.errors.append("<br />").append("Раздел-8. Счёт-фактура № ")
						.append(entityRazdel11.getNomSchFProd()).append("<br />")
						.append("В стоитости более двух знаков после запятой<br /> ==========================================");
				logger.info("Раздел-9. Счёт-фактура № " + entityRazdel11.getNomSchFProd());
				logger.info("В стоитости более двух знаков после запятой");
				logger.info("==========================================");
			}

			if (entityRazdel11.getSumNDSSchF() != null
					&& !ParamTesting.check2NumberAfterDot(entityRazdel11.getSumNDSSchF())) {
				MakeAllReports.errors.append("<br />").append("Раздел-8. Счёт-фактура № ")
						.append(entityRazdel11.getNomSchFProd()).append("<br />")
						.append("В стоитости более двух знаков после запятой<br /> ==========================================");
				logger.info("Раздел-9. Счёт-фактура № " + entityRazdel11.getNomSchFProd());
				logger.info("В стоитости более двух знаков после запятой");
				logger.info("==========================================");
			}
		}
		return entityRazdel11s;
	}
}
