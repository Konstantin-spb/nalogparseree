package xml.handlers;

import entity.EntityRazdel10;
import entity.subEntities.SubEntities10Prod;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Sheet;
import xml.ReadOneList;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

/**
 * Created by konstantin on 02.07.15.
 */
public class Razdel10 {

    public List<EntityRazdel10> parseRazdel10(InputStream in) {

        List<EntityRazdel10> entityRazdel10s = new ArrayList<EntityRazdel10>();
//        InputStream in;
        HSSFWorkbook wb = null;
        try {
//            in = new FileInputStream(name);
            wb = new HSSFWorkbook(in);
        } catch (IOException e) {
            e.printStackTrace();
        }
        Sheet sheet = wb.getSheetAt(0);
        List<ArrayList> arrayLists = new ReadOneList().read(sheet);

        //Удалить первые 3 строки из коллекции
        for (int del = 0; del < 2; del++) {
            arrayLists.remove(0);
        }

        String result = "";
        for (ArrayList arrayList : arrayLists) {
            int j = 0;
            for (Object ob : arrayList) {
                if (ob != null) {
                    result += j + "#" + ob + "\t";
                }
                j++;
            }
            result += "\n";
        }
//        System.out.println(result);

// Записываем номена накладных в set коллекцию, получаем список уникальных номеров накладных
        Set<String> stringSet = new TreeSet<String>();
        String[] str;
        String temp;
        for (List array : arrayLists) {
            try {
                stringSet.add(array.get(2).toString());
            } catch (NullPointerException e) {
                e.printStackTrace();
            }
        }
//        System.out.println();

        ArrayList<ArrayList<ArrayList>> lists = new ArrayList<ArrayList<ArrayList>>();
        Integer i = 0;
        for (String s : stringSet) {
            lists.add(new ArrayList<ArrayList>());
            int j = 0;
            for (ArrayList arrayList : arrayLists) {
                temp = arrayList.get(2).toString();
                if (s.equals(temp)) {
                    lists.get(i).add(arrayLists.get(j));
                }
                j++;
            }
            i++;
        }

        for (ArrayList<ArrayList> list : lists) {
            entityRazdel10s.add(new EntityRazdel10());

            int ind = lists.indexOf(list);

            entityRazdel10s.get(ind).setDataVyst(list.get(0).get(1).toString());
            entityRazdel10s.get(ind).setNomSchFPokup(list.get(0).get(2).toString());
            entityRazdel10s.get(ind).setDataSchFPokup(list.get(0).get(3).toString());
            entityRazdel10s.get(ind).setKodVidOper(list.get(0).get(0).toString());

            entityRazdel10s.get(ind).setInnPokup(list.get(0).get(11).toString());
            entityRazdel10s.get(ind).setKppPokup(list.get(0).get(12).toString());

            int count2 = 0;
            for (ArrayList a : list) {

                entityRazdel10s.get(ind).getProds().add(new SubEntities10Prod());
                entityRazdel10s.get(ind).getProds().get(count2).setInnProd(a.get(14).toString());
                entityRazdel10s.get(ind).getProds().get(count2).setKppProd(a.get(15).toString());
                entityRazdel10s.get(ind).getProds().get(count2).setNomSchFOtProdProd(a.get(16).toString());
                entityRazdel10s.get(ind).getProds().get(count2).setDataSchFOtProdProd(a.get(17).toString());
                entityRazdel10s.get(ind).getProds().get(count2).setOkvProd(a.get(19).toString());
                entityRazdel10s.get(ind).getProds().get(count2).setStoimTovSchFVs(a.get(20).toString());
                entityRazdel10s.get(ind).getProds().get(count2).setSumNDSSchF(a.get(21).toString());

                count2++;
            }
        }

        arrayLists = null;

        i = 0;
        return entityRazdel10s;
    }
}
