package xml.handlers;

import ejb.MakeAllReports;
import entity.EntityRazdel9;
import org.apache.log4j.Logger;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Sheet;
import tests.ParamTesting;
import xml.ReadOneList;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by konstantin on 23.04.15.
 */
public class Razdel9 {

	static final Logger logger = Logger.getLogger(Razdel9.class);

	public List<EntityRazdel9> parseRazdel9(InputStream in) {

		List<EntityRazdel9> entityRazdel9s = new ArrayList<EntityRazdel9>();
		// InputStream in;
		HSSFWorkbook wb = null;
		try {
			// in = new FileInputStream(fileName);
			wb = new HSSFWorkbook(in);
		} catch (IOException e) {
			e.printStackTrace();
		}

		Integer i = 0;
		Sheet sheet = wb.getSheetAt(0);
		List<ArrayList> arrayLists = new ReadOneList().read(sheet);

		// Удалить первые 3 строки из коллекции
		for (int del = 0; del < 2; del++) {
			arrayLists.remove(0);
		}

		for (List array : arrayLists) {

			entityRazdel9s.add(new EntityRazdel9());
			String result = "";
			int j = 0;
			for (Object s : array) {

				result += j + "#" + s + "\t";

				if (s != null) {

					String temp = (String) s;
					temp = temp.replaceAll("\\\"", "").trim();

					try {
						switch (j) {
							// Код вида операции (<КодВидОпер>01</КодВидОпер>)
						case 0:
							entityRazdel9s.get(i).setKodVidOper(temp);
							break;

						// Номер счета-фактуры продавца(НомСчФПрод)
						case 1:
							entityRazdel9s.get(i).setNomSchFProd(temp);
							break;

						// дата счета-фактуры продавца(ДатаСчФПрод)
						case 2:
							entityRazdel9s.get(i).setDataSchFProd(temp);
							break;

							// номер счета-фактуры продавца(НомСчФПрод)
						case 3:
							entityRazdel9s.get(i).setNomIsprSchF(temp);
							break;

							// дата счета-фактуры продавца(ДатаСчФПрод)
						case 4:
							entityRazdel9s.get(i).setDataIcprSchF(temp);
							break;

						// Номер корректировочнного счета-фактуры продавца(НомКСчФПрод)
						case 5:
							entityRazdel9s.get(i).setNomKSchFProd(temp);
							break;

						//  дата корректировочнного счета-фактуры продавца(ДатаКСчФПрод)
						case 6:
							entityRazdel9s.get(i).setDataKSchFProd(temp);
							break;

						// Номер исправления корректировочного счета-фактуры
						// продавца (НомИспрКСчФ)
						case 7:
							entityRazdel9s.get(i).setNomIsprKSchF(temp);
							break;

						// дата исправления корректировочного счета-фактуры
						// продавца (ДатаИспрКСчФ)
						case 8:
							entityRazdel9s.get(i).setDataIsprKschF(temp);
							break;

						// Наименование покупателя
						case 9:
							break;

						// инн покупателя (<СвПокуп> <СведЮЛ ИННЮЛ=)
						case 10:
							entityRazdel9s.get(i).setInnPokup(temp);
							break;

						// кпп покупателя (КПП)
						case 11:
							entityRazdel9s.get(i).setKppPokup(temp);
							break;

						// Наименование посредника
						case 12:
							break;

						// инн  посредника (<СвПокуп> <СведЮЛ ИННЮЛ=)
						case 13:
							entityRazdel9s.get(i).setInnPosrednik(temp);
							break;

						// кпп  посредника(КПП)
						case 14:
							entityRazdel9s.get(i).setKppPosrednik(temp);
							break;

						// Номер документа, подтверждающего оплату(НомДокПдтвОпл)
						case 15:
							entityRazdel9s.get(i).setNomDokPdtvOpl(temp);
							break;

						// дата документа, подтверждающего оплату(ДатаДокПдтвОпл)
						case 16:
							entityRazdel9s.get(i).setDataDokPdtvOpl(temp);
							break;

						// Наименование валюты
						case 17:
							break;

						// код валюты(ОКВ)
						case 18:
							entityRazdel9s.get(i).setOkv(temp);
							break;

						// Стоимость продаж по счету-фактуре,разница стоимости
						// по корректиро-вочному счету-фактуре (включая НДС) в
						// валюте счет-фактуры(СтоимПродСФВ)
						case 19:
							entityRazdel9s.get(i).setStoimProdSFV(temp);
							break;

						// "Стоимость продаж по счету-фактуре,разница стоимости
						// по корректиро-вочному счету-фактуре (включая НДС) в
						// рублях и копейках"(СтоимПродСФВ)
						case 20:
							entityRazdel9s.get(i).setStoimProdSF(temp);
							break;

						// Стоимость продаж, облагаемых налогом, по
						// счету-фактуре,разница стоимости по корректиро-вочному
						// счету-фактуре (без НДС) в рублях и копейках,по ставке
						// 18 про-цен-тов(СтоимПродСФ18)
						case 21:
							if (Double.parseDouble(temp) > 0) {
								entityRazdel9s.get(i).setStoimProdSF18_10(temp);
							}
							break;

						// Стоимость продаж, облагаемых налогом, по
						// счету-фактуре,разница стоимости по корректиро-вочному
						// счету-фактуре (без НДС) в рублях и копейках,по ставке
						// 10 про-цен-тов (СтоимПродСФ10)
						case 22:
							if (Double.parseDouble(temp) > 0) {
								entityRazdel9s.get(i).setStoimProdSF18_10(temp);
							}
							break;

						// Стоимость продаж, облагаемых налогом, по
						// счету-фактуре,разница стоимости по корректиро-вочному
						// счету-фактуре (без НДС) в рублях и копейках,по ставке
						// 0 про-цен-тов (СтоимПродСФ0)
						case 23:
							if (Double.parseDouble(temp) > 0) {
								entityRazdel9s.get(i).setStoimProdSF0(temp);
							}
							break;

						// Сумма НДС по счету-фактуре, разница стоимости по 
						// корректировочному счету-фактуре в рублях и
						// копейках,по ставке 18 про-цен-тов
						case 24:
							// 18%
							entityRazdel9s.get(i).setSumNDSSF(temp);
							break;

						// Сумма НДС по счету-фактуре, разница стоимости по 
						// корректировочному счету-фактуре в рублях и
						// копейках,по ставке 10 про-цен-тов(СумНДССФ18)
						case 25:
							if (Double.parseDouble(temp) > 0) {
								entityRazdel9s.get(i).setSumNDSSF(temp);
							}
							break;

						// Стоимость продаж, освобож-даемых от налога, по
						// счету-фактуре, разница стоимости по
						// корректиро-вочному счету-фактуре в рублях и копейках(СтоимПродОсв)
						case 26:
							if (Double.parseDouble(temp) > 0) {
								entityRazdel9s.get(i).setStoimProdOsv(temp);
							}
							break;
						}

					} catch (NumberFormatException e) {
						e.printStackTrace();
					}
				}
				j++;
			}
			// System.out.println(result);
			result = null;

			i++;
		}
		arrayLists = null;

		i = 0;

		logger.info("Начало тестирования");

		for (EntityRazdel9 entityRazdel9 : entityRazdel9s) {

			// ParamTesting.razdel9testing(entityRazdel9);

			if (entityRazdel9.getInnPokup() != null && !entityRazdel9.getInnPokup().isEmpty()
					&& !ParamTesting.innTesting(entityRazdel9.getInnPokup())) {
				MakeAllReports.errors.append("<br />").append("Раздел-8. Счёт-фактура № ")
						.append(entityRazdel9.getNomSchFProd()).append("<br />")
						.append("В стоитости более двух знаком после запятой<br /> ==========================================");
				logger.info("Раздел-9. Счёт-фактура № " + entityRazdel9.getNomSchFProd());
				logger.info("ИНН продавца некорректный");
				logger.info(" ==========================================");
			}

			if (entityRazdel9.getKppPokup() != null && !entityRazdel9.getKppPokup().isEmpty()
					&& !ParamTesting.kppTesting(entityRazdel9.getKppPokup(), entityRazdel9.getInnPokup())) {
				MakeAllReports.errors.append("<br />").append("Раздел-8. Счёт-фактура № ")
						.append(entityRazdel9.getNomSchFProd()).append("<br />").append("КПП продавца некорректный ")
						.append(entityRazdel9.getKppPokup()).append("<br />")
						.append(" ==========================================");
				logger.info("Раздел-9. Счёт-фактура № " + entityRazdel9.getNomSchFProd());
				logger.info("КПП продавца некорректный" + " ---> " + entityRazdel9.getKppPokup());
				logger.info(" ==========================================");
			}

			if (entityRazdel9.getInnPosrednik() != null && !entityRazdel9.getInnPosrednik().isEmpty()
					&& !entityRazdel9.getInnPosrednik().isEmpty()
					&& !ParamTesting.innTesting(entityRazdel9.getInnPosrednik())) {
				MakeAllReports.errors.append("<br />").append("Раздел-8. Счёт-фактура № ")
						.append(entityRazdel9.getNomSchFProd()).append("<br />")
						.append("В стоитости более двух знаком после запятой ").append(entityRazdel9.getInnPosrednik())
						.append("<br />").append(" ==========================================");
				logger.info("Раздел-9. Счёт-фактура № " + entityRazdel9.getNomSchFProd());
				logger.info("ИНН посредника некорректный" + " ---> " + entityRazdel9.getInnPosrednik());
				logger.info(" ==========================================");
			}

			if (entityRazdel9.getKppPosrednik() != null && !entityRazdel9.getKppPosrednik().isEmpty()
					&& !ParamTesting.kppTesting(entityRazdel9.getKppPosrednik(), entityRazdel9.getInnPosrednik())) {
				MakeAllReports.errors.append("<br />").append("Раздел-8. Счёт-фактура № ")
						.append(entityRazdel9.getNomSchFProd()).append("<br />")
						.append("В стоитости более двух знаком после запятой<br /> ==========================================");
				logger.info("Раздел-9. Счёт-фактура № " + entityRazdel9.getNomSchFProd());
				logger.info("КПП посредника некорректный");
				logger.info("==========================================");
			}

			if (entityRazdel9.getStoimProdSF0() != null && !entityRazdel9.getStoimProdSF0().isEmpty()
					&& !ParamTesting.check2NumberAfterDot(entityRazdel9.getStoimProdSF0())) {
				MakeAllReports.errors.append("<br />").append("Раздел-8. Счёт-фактура № ")
						.append(entityRazdel9.getNomSchFProd()).append("<br />")
						.append("В стоитости более двух знаком после запятой<br /> ==========================================");
				logger.info("Раздел-9. Счёт-фактура № " + entityRazdel9.getNomSchFProd());
				logger.info("В стоитости более двух знаком после запятой");
				logger.info("==========================================");
			}
			if (entityRazdel9.getStoimProdSF() != null && !entityRazdel9.getStoimProdSF().isEmpty()
					&& !ParamTesting.check2NumberAfterDot(entityRazdel9.getStoimProdSF())) {
				MakeAllReports.errors.append("<br />").append("Раздел-8. Счёт-фактура № ")
						.append(entityRazdel9.getNomSchFProd()).append("<br />")
						.append("В стоитости более двух знаком после запятой<br /> ==========================================");
				logger.info("Раздел-9. Счёт-фактура № " + entityRazdel9.getNomSchFProd());
				logger.info("В стоитости более двух знаком после запятой");
				logger.info("==========================================");
			}
			if (entityRazdel9.getStoimProdOsv() != null && !entityRazdel9.getStoimProdOsv().isEmpty()
					&& !ParamTesting.check2NumberAfterDot(entityRazdel9.getStoimProdOsv())) {
				MakeAllReports.errors.append("<br />").append("Раздел-8. Счёт-фактура № ")
						.append(entityRazdel9.getNomSchFProd()).append("<br />")
						.append("В стоитости более двух знаком после запятой<br /> ==========================================");
				logger.info("Раздел-9. Счёт-фактура № " + entityRazdel9.getNomSchFProd());
				logger.info("В стоитости более двух знаком после запятой");
				logger.info("==========================================");
			}
			if (entityRazdel9.getStoimProdSF18_10() != null && !entityRazdel9.getStoimProdSF18_10().isEmpty()
					&& !ParamTesting.check2NumberAfterDot(entityRazdel9.getStoimProdSF18_10())) {
				MakeAllReports.errors.append("<br />").append("Раздел-8. Счёт-фактура № ")
						.append(entityRazdel9.getNomSchFProd()).append("<br />")
						.append("В стоитости более двух знаком после запятой<br /> ==========================================");
				logger.info("Раздел-9. Счёт-фактура № " + entityRazdel9.getNomSchFProd());
				logger.info("В стоитости более двух знаком после запятой");
				logger.info("==========================================");
			}
			if (entityRazdel9.getStoimProdSFV() != null && !entityRazdel9.getStoimProdSFV().isEmpty()
					&& !ParamTesting.check2NumberAfterDot(entityRazdel9.getStoimProdSFV())) {
				MakeAllReports.errors.append("<br />").append("Раздел-8. Счёт-фактура № ")
						.append(entityRazdel9.getNomSchFProd()).append("<br />")
						.append("В стоитости более двух знаком после запятой<br /> ==========================================");
				logger.info("Раздел-9. Счёт-фактура № " + entityRazdel9.getNomSchFProd());
				logger.info("В стоитости более двух знаком после запятой");
				logger.info("==========================================");
			}
		}

		logger.info("Конец тестирования");

		return entityRazdel9s;

	}

}
