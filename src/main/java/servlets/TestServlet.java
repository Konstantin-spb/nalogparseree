package servlets;

import ejb.MakeAllReports;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileItemFactory;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;

import javax.ejb.EJB;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Iterator;
import java.util.List;

//@MultipartConfig
public class TestServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;

	private static final int BYTES_DOWNLOAD = 1024;

	@EJB
	MakeAllReports makeAllReports;

	public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
		doPost(request, response);
	}

	public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException {

		response.setContentType("text/html; charset=UTF-8");
//		response.setContentType("application/zip");

		PrintWriter out = response.getWriter();

//		out.println("Hello<br/>");

		boolean isMultipartContent = ServletFileUpload.isMultipartContent(request);
		if (!isMultipartContent) {
			out.println("You are not trying to upload<br/>");
			return;
		}
		out.println("You are trying to upload<br/>");

		FileItemFactory factory = new DiskFileItemFactory();
		ServletFileUpload upload = new ServletFileUpload(factory);
		try {
			List<FileItem> fields = upload.parseRequest(request);
			out.println("Number of fields: " + fields.size() + "<br/><br/>");
			Iterator<FileItem> it = fields.iterator();
			if (!it.hasNext()) {
				out.println("No fields found");
				return;
			}
			String zipFilename = makeAllReports.go(fields);
			if (MakeAllReports.errors.toString().isEmpty()) {
				out.println("<h1>Программа закончила свою работу</h1>");
				out.println("<h1>Имя файла</h1>" + zipFilename);
				out.println("<p><a href=\"/files/" + zipFilename + "\">Ссылка на Ваш архив с отчетом</a></p>");
			} else {
				out.println("<p><a href=\"/files/" + zipFilename + "\">Можете забрать неправильный архив здесь, если он вам всё таки нужен.</a></p>");
				out.print(MakeAllReports.errors.toString());
			}
        } catch (FileUploadException e) {
			e.printStackTrace();
		}
	}
}