package models;

import javax.faces.bean.ManagedBean;

@ManagedBean(name = "company")
public class Company {

    private long ifns;
    private long inn;
    private long kpp;
    private String currentDate;

    public long getIfns() {
        return ifns;
    }

    public void setIfns(long ifns) {
        this.ifns = ifns;
    }

    public long getInn() {
        return inn;
    }

    public void setInn(long inn) {
        this.inn = inn;
    }

    public long getKpp() {
        return kpp;
    }

    public void setKpp(long kpp) {
        this.kpp = kpp;
    }

    public String getCurrentDate() {
        return currentDate;
    }

    public void setCurrentDate(String currentDate) {
        this.currentDate = currentDate;
    }
}
