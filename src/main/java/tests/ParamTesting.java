package tests;

import entity.EntityRazdel9;
import org.apache.log4j.Logger;

public class ParamTesting {

    static final Logger logger = Logger.getLogger(ParamTesting.class);

    public static boolean checkKodVidOper(String s) {
        if (s.length() == 2)
            return true;
        else return false;
    }

    public static boolean innTesting(String s) {
        //12, 10
        if( s.length() == 10 || s.length() == 12) {
            return true;
        } else {
            return false;
        }
    }

    public static boolean kppTesting(String kpp, String inn) {
        //9 знаков или null

        if (inn.length() == 10 && kpp.length() == 9) {
            return true;
        } else if (inn.length() == 12 && (kpp.length() == 9 || kpp.isEmpty())) {
            return true;
        } else {
            return false;
        }

//        if( kpp.length() == 9 || kpp.isEmpty()) {
//            return true;
//        } else  {
//            return false;
//        }
    }

    public static boolean okvTesting(String s) {
        //3 знака
        if( s.length() == 3) {
            return true;
        } else {
            return false;
        }
    }

    public static boolean nomerIstrSchFTesting(String s) {
        //3 знака
        if( s.length() <= 3) {
            return true;
        } else {
            return false;
        }
    }

    public static boolean check2NumberAfterDot(String s) {
        if (s.contains(".")) {
            String[] str = s.split("\\.");
            if (str[1].length() <= 2) {
                return true;
            } else {
                return false;
            }
        } else return true;
    }

    public static boolean checkNomTD(String s) {

        boolean lengthIsNorm = false;

        if(s.contains(";")) {
            String[] str = s.split(";");
            for (String s1 : str) {
                if(s1.trim().length() < 30) {
                    lengthIsNorm = true;
                } else {
                    lengthIsNorm = false;
                }
            }
        } else if(!s.contains(";") && s.length() > 5 && s.length() < 30) {
            lengthIsNorm = true;
        }

        return lengthIsNorm;
    }

    public static boolean razdel9testing ( EntityRazdel9 entityRazdel9) {

        boolean testResult = false;

        if ((entityRazdel9.getInnPokup().length() == 12 || entityRazdel9.getInnPokup().length() == 10) && entityRazdel9.getInnPokup() != null) {
            if (entityRazdel9.getInnPokup().length() == 12 && entityRazdel9.getKppPokup().isEmpty()) {
//                logger.info(entityRazdel9.getNomSchFProd() + " ИНН12/КПП покупателя - ошибка");
                testResult = true;
            } else if (entityRazdel9.getInnPokup().length() == 10 && entityRazdel9.getKppPokup().length() == 9) {
//                logger.info(entityRazdel9.getNomSchFProd() + " ИНН10/КПП покупателя - ошибка");
                testResult = true;
            }
        } else {
            logger.info(entityRazdel9.getNomSchFProd() + " Содержит ошибки");
        }

        return testResult;
    }
}
