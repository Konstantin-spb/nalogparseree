<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.Date" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"
      xmlns:h="http://java.sun.com/jsf/html"
      xmlns:f="http://java.sun.com/jsf/core"
      xmlns:p="http://primefaces.org/ui">
<head>
    <meta http-equiv="Content-Type" content="text/html">
    <title>Upload Page</title>
</head>
<body>
<form name="form" action="/nds" method="post" enctype="multipart/form-data">

    <input name="ifns"      type="text" value="7834"> Код ИФНС<br />
    <input name="inn"       type="text" value="7816151039"> ИНН<br />
    <input name="kpp"       type="text" value="783450001"> КПП<br />
    <input name="calendar"  type="date" value="<%=new SimpleDateFormat("yyyy-MM-dd").format(new Date())%>" /> Выберите дату<br /><br />
    <input name="knd"      type="text" value="1151001"> Классификатор налоговой документации(КНД)<br />
    <input name="poMestu"      type="text" value="213"> Код места, по которому представляется документ<br />
    <input name="nomKorr"      type="text" value="0"> Номер корректировки<br />
    <input name="okved"      type="text" value="20.30.1"> Код вида экономической деятельности по классификатору ОКВЭД<br />
    <input name="naimOrg"      type="text" value="Общество с ограниченной ответственностью &quot;СоюзБалтКомплект&quot;"> Наименование организации<br />
    <input name="familiya"      type="text" value="Кошицкий"> Фамилия<br />
    <input name="imya"      type="text" value="Ярослав"> Имя<br />
    <input name="otchestvo"      type="text" value="Иванович"> Отчество<br />
    <input name="prPodp"      type="text" value="1"> Признак лица, подписавшего документ.Принимает значение:1 - налогоплательщик, налоговый агент |2 - представитель налогоплательщика, налогового агента<br />
    <input name="oktmo"      type="text" value="50015268"> Код ОКТМО(Общероссийский классификатор территорий муниципальных образований.)<br />
    <input name="oktmo"      type="text" value="18210301000011000110"> Код бюджетной классификации<br />
    <input name="kvartal"   type="radio" value="1"> Первый квартал
    <input name="kvartal"   type="radio" value="2"> Второй квартал
    <input name="kvartal"   type="radio" value="3" checked> Третий квартал
    <input name="kvartal"   type="radio" value="4"> Четвертый

    <br/>
    <br/>
    Files to upload:
    <br/>

    <h3> Razdel 8</h3>
    <input type="file" name="file8">
    <br/>

    <h3> Razdel 9</h3>
    <input type="file" name="file9">
    <br/>

    <h3> Razdel 10</h3>
    <input type="file" name="file10">
    <br/>

    <h3> Razdel 11</h3>
    <input type="file" name="file11">
    <br/>
    <br/>
    <input type="submit" value="Upload">
</form>
</body>
</html>